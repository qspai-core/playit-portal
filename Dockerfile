FROM node:12-alpine as build
 
RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

RUN npm install

WORKDIR /usr/src/app/frontend

RUN ls
RUN rm -r ./build/*
RUN npm install
RUN npm run build

WORKDIR /usr/src/app

EXPOSE 3000 

CMD ["npm", "run", "start"]


#RUN npm run build

# Prepare nginx
#FROM nginx:1.16.0-alpine
#COPY --from=build /app/build /usr/share/nginx/html
#RUN rm /etc/nginx/conf.d/default.conf
#COPY nginx/nginx.conf /etc/nginx/conf.d

#EXPOSE 80
#CMD ["nginx", "-g", "daemon off;"]
