module.exports = { 
    successfullyResponse(res, data){
        return res.status(200).json({
            success: true,
            data: data
        });
    },
    unprocessableEntityResponse(res, errors){
        return res.status(422).json({
            errors: {
                "success": false,
                "error": {
                    "code": 1,
                    "message": "Campos inválidos",
                    "errors": errors.array()
                }
            }
        });
    },
    basicResponse(res, err, data){
        if(err)
            return res.status(400).json({
                success: false,
                error: err
            });

        return res.status(200).json({
            success: true, 
            data: data
        });
    }
}
