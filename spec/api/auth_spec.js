const auth = require('../../api/controllers/auth'),
    MongoClient = require("mongodb").MongoClient,
    testConfig = require("../../config/settings"),
    log4js = require("log4js"),
    ObjectID = require("mongodb").ObjectID;

describe('Suite of auth tests', function () {

    it('should login with a valid email and password', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }
            auth.init(log4js, dbClient);

            const json = { email: '170221032@estudantes.ips.pt', password: "Teste123" };
            auth.validate(json.email, json.password, function (error, user) {
                if (error) {
                    fail(error);
                }
                done();
            })
        });
    });

    it('should not login with an invalid email and password', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }
            auth.init(log4js, dbClient);

            const json = { email: 'asd@asd.com', password: "asd" };
            auth.validate(json.email, json.password, function (error, user) {
                if (error) {
                    done();
                }
                else {
                    fail('authenticated with invalid email');
                }

            })
        });
    });

});
