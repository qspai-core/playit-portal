const game = require('../../api/controllers/game'),
    MongoClient = require("mongodb").MongoClient,
    testConfig = require("../../config/settings"),
    log4js = require("log4js"),
    ObjectID = require("mongodb").ObjectID;

describe('Suite of game tests', function () {

    it('should get all the games available', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            game.init(log4js, dbClient);

            try {
                var result = await game.getAllGames(0, 2);
                if (result[0]._id.equals(ObjectID('5fce5526e4a2732c4c229752'))) {
                    done();
                }
            }
            catch {
                fail("couldn't get all games")
            }

        });
    });

    it('should get the details of a game', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            game.init(log4js, dbClient);

            const gameId = ObjectID('5fce5526e4a2732c4c229752')

            try {
                var result = await game.getGameById(gameId)
                if (result._id.equals(gameId)) {
                    done();
                }
            }
            catch {
                fail("couldn't get the game")
            }
        });
    })

    it('should deactivate and activate a game', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            game.init(log4js, dbClient);

            const gameId = ObjectID('5fce5526e4a2732c4c229752')

            //deactivate
            game.setActive(gameId, false, function (error) {
                if (error) {
                    fail(error)
                }
            })

            //check if is active
            try {
                var result = await game.getGameById(gameId);
                if (result.isActive) {
                    fail('game is still active')
                }
            }
            catch {
                fail("couldn't get the game")
            }

            //activate again
            game.setActive(gameId, true, function (error) {
                if (error) {
                    fail(error)
                }
            })

            //check if is active and end
            try {
                var res = await game.getGameById(gameId);
                if (res.isActive) {
                    done()
                }
            }
            catch {
                fail("couldn't get the game")
            }
        });
    });

    it('should not deactivate or activate a game that does not exists', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            game.init(log4js, dbClient);

            const gameId = '0'

            //deactivate
            game.setActive(gameId, false, function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('the invalid game was deactivated')
                }
            })
        });
    });
});