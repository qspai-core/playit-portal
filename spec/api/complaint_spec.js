const complaint = require('../../api/controllers/complaint'),
    MongoClient = require("mongodb").MongoClient,
    testConfig = require("../../config/settings"),
    log4js = require("log4js"),
    ObjectID = require("mongodb").ObjectID;

describe('Suite of complaint tests', function () {

    it('should return all ccomplaints', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            complaint.init(log4js, dbClient);

            var complaintId = ObjectID('5fcecc25e4a2732c4c229761')
            try {
                var result = await complaint.getAllComplaint(0, 2);
                if (result[0]._id.equals(complaintId)) {
                    done();
                }
            }
            catch {
                fail("couldn't get all complaints")
            }
        });
    });

    it('should return one complaint by id', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            complaint.init(log4js, dbClient);

            var complaintId = ObjectID('5fcecc25e4a2732c4c229761')
            try {
                var result = await complaint.getComplaintById(complaintId);
                if (result._id.equals(complaintId)) {
                    done();
                }
            }
            catch {
                fail("couldn't get the complaint")
            }
        });
    })

    it('should create a response to a complaint', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            complaint.init(log4js, dbClient);
            var complaintId = ObjectID('5fcecc25e4a2732c4c229761')
            var userId = ObjectID('5fcec984e4a2732c4c22975f')
            var responseText = 'response test'
            complaint.addResponse(complaintId, userId, responseText, function (error) {
                if (error) {
                    fail(error);
                }
            })

            try {
                var result = await complaint.getComplaintById(complaintId);
                if (result.responses[result.responses.length - 1].text === responseText) {
                    done();
                }
            }
            catch {
                fail("couldn't get the complaint")
            }
        });
    })

    it('should not create a response to an invalid complaint', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            complaint.init(log4js, dbClient);
            var complaintId = '1'
            var userId = ObjectID('5fcec984e4a2732c4c22975f')
            var responseText = 'response test'
            complaint.addResponse(complaintId, userId, responseText, function (error) {
                if (error) {
                    done();
                }
                else {
                    fail('a response was created to an invalid complaint')
                }
            })
        });
    })

    it('should archive and unarchive the complaint', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            complaint.init(log4js, dbClient);
            var complaintId = ObjectID('5fcecc25e4a2732c4c229761')
            var userId = ObjectID('5fcec984e4a2732c4c22975f')

            //archive the complaint
            complaint.setArchived(complaintId, userId, true, function (error) {
                if (error) {
                    fail(error)
                }
            })

            //check if is not archived
            try {
                var result = await complaint.getComplaintById(complaintId);
                if (!result.isArchived) {
                    fail('complaint was not archived')
                }
            }
            catch {
                fail("couldn't get the complaint")
            }

            //unarchive 
            complaint.setArchived(complaintId, userId, false, function (error) {
                if (error) {
                    fail(error)
                }
            })

            //check if is unarchived and end
            try {
                var res = await complaint.getComplaintById(complaintId);
                if (!res.isArchive) {
                    done()
                }
            }
            catch {
                fail("couldn't get the complaint")
            }
        });
    })

    it('should not archive or unarchive an invalid complaint', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            complaint.init(log4js, dbClient);
            var complaintId = '1'
            var userId = ObjectID('5fcec984e4a2732c4c22975f')

            //archive the complaint
            complaint.setArchived(complaintId, userId, true, function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('an invalid complaint was modified')
                }
            })
        });
    })
});
