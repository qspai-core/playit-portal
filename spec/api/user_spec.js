const user = require('../../api/controllers/user'),
    auth = require('../../api/controllers/auth'),
    MongoClient = require("mongodb").MongoClient,
    testConfig = require("../../config/settings"),
    log4js = require("log4js"),
    ObjectID = require("mongodb").ObjectID;

describe('Suite of user tests', function () {

    it('should return all users', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);

            try {
                var result = await user.getAllUsers(0, 2);
                if (result[0]._id.equals(ObjectID('5fce1d7e508dbb7798cf50e5'))) {
                    done();
                }
            }
            catch {
                fail("couldn't get all users")
            }
        });
    });

    it('should get details from a user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);
            try {
                var result = await user.getUserByEmail('paulo.fournier@gmail.com');
                if (result.email === 'paulo.fournier@gmail.com') {
                    done();
                }
            }
            catch {
                fail("couldn't get all users")
            }
        });
    })

    it('should modify data from a user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);

            user.setUser('5fcecbefe4a2732c4c229760', 'Nome Alterado', new Date(), '', '5fcec984e4a2732c4c22975f', function (error) {
                if (error) {
                    fail(error)
                }
                done()
            })
        });
    })

    it('should not modify data from an invalid user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);

            user.setUser('1', '', new Date(), '', '', function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('an invalid user was modified')
                }
            })
        });
    })

    it('should not modify data from a user with an invalid roleID', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);

            user.setUser('5fcecbefe4a2732c4c229760', 'Nome Alterado', new Date(), '', '1', function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('user was modified with an invalid roleId')
                }
            })
        });
    })

    it('should not modify data from a user with an inexistent roleId', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);

            user.setUser('5fcecbefe4a2732c4c229760', 'Nome Alterado', new Date(), '', '000000000000000000000000', function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('user was modified with an inexistent roleId')
                }
            })
        });
    })

    it('should change the password of a user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);
            auth.init(log4js, dbClient)

            const json = { email: 'john.doe@gmail.com', password: "Passw0rd" };
            var userId = ''

            //validate the first password
            await auth.validate(json.email, json.password, function (error, user) {
                if (error) {
                    fail(error);
                }
                done();
            })

            //get user id
            try {
                var result = await user.getUserByEmail(json.email);
                userId = result._id
            }
            catch {
                fail("couldn't get the user")
            }

            //change Password
            var newPassword = "Teste123"
            await user.setPassword(userId, newPassword, function (error) {
                if (error) {
                    fail(error)
                }
            })

            //validate if new password is working
            await auth.validate(json.email, newPassword, function (error, user) {
                if (error) {
                    fail(error);
                }
                done();
            })

            //get back to old password
            await user.setPassword(userId, json.password, function (error) {
                if (error) {
                    fail(error)
                }
            })

        });
    })

    it('should not change the password of a invalid user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);
            auth.init(log4js, dbClient)

            //change Password
            await user.setPassword('1', 'newPassword', function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('password changed with an invalid user')
                }
            })

        });
    })

    it('should not change the password of a inexistent user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);
            auth.init(log4js, dbClient)

            //change Password
            await user.setPassword('000000000000000000000000', 'newPassword', function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('password changed with an inexistent user')
                }
            })

        });
    })

    it('should deactivate and activate a user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);
            const userId = ObjectID('5fcecbefe4a2732c4c229760')

            //deactivate
            user.setActive(userId, false, function (error) {
                if (error) {
                    fail(error)
                }
            })

            //check if is active
            try {
                var result = await user.getUserById(userId);
                if (result.isActive) {
                    fail('user is still active')
                }
            }
            catch {
                fail("couldn't get the user")
            }

            //activate again
            user.setActive(userId, true, function (error) {
                if (error) {
                    fail(error)
                }
            })

            //check if is active and end
            try {
                var res = await user.getUserById(userId);
                if (res.isActive) {
                    done()
                }
            }
            catch {
                fail("couldn't get the user")
            }
        });
    });

    it('should not deactivate or activate an invalid user', function (done) {
        MongoClient(testConfig.mongo.connectionString, {
            useUnifiedTopology: true,
        }).connect(async function (err, dbClient) {
            if (err) {
                console.log("MongoDB connection error: %s", err.message);
                fail("could not connect to the mongoDB");
            }

            user.init(log4js, dbClient);
            const userId = '1'

            //deactivate
            user.setActive(userId, false, function (error) {
                if (error) {
                    done()
                }
                else {
                    fail('an inexistent user was modified')
                }
            })
        });
    })
});