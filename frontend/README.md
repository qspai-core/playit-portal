### Estrutura das pastas Structure

#### assets

Esta pasta é onde os ficheiros \_shared.scss se encontram, onde irão ser colocádos todos os estilos genericos bem como as varieis globais de scss.

#### store

Pasta onde se encontram as actions e reducers.

#### pages

Pasta onde estão guardadas todas as páginas da aplicação.

Podem existir casos em que alguns componentes são especificos da página, nesse caso é criada uma pasta componentes e colocados lá esses componentes.

#### components

Onde todos os componentes genericos irão estar.

#### router

Nesta pasta está o mechanismo de rotas privadas e publicas.
A configuração encontra-se no ficheiro routes.js.

As propriedades mais importantes são:

-   **layout** - Layout que a rota irá mostar.
-   **isPrivate** - Se é preciso autenticação ou não.

### Linter

Configuração para ficheiro .eslintrc:

```
{
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:sonarjs/recommended",
    "plugin:prettier/recommended"
  ],
  "plugins": ["prettier", "sonarjs"],
  "rules": {
    "prettier/prettier": ["error"],
    "no-empty-function": "error",
    "no-magic-numbers": "warn",
    "no-multi-spaces": "error",
    "no-return-await": "error",
    "no-useless-return": "error",
    "require-await": "error",
    "arrow-spacing": "error",
    "prefer-const": "error",
    "prefer-template": "error"
  },
  "settings": {
    "react": {
      "version": "detect"
    }
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  }
}

```

### Code Formatter (Prettier)

Configuração para ficheiro .prettierrc:

```
{
  "semi": true,
  "trailingComma": "all",
  "singleQuote": true,
  "printWidth": 120,
  "tabWidth": 2
}
```

### Visual Studio Code

Configuração para ficheiro settings.json:

```
{
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "eslint.validate": ["javascript", "javascriptreact"]
}
```
