let ContributorAccessGroup = (function() {
    let formatToJson = function() {
        return this.accesses.map(e => e.formatToJson());
    };

    return function ContributorAccessGroup(accesses) {
        this.accesses = accesses;
        this.formatToJson = formatToJson;
    };
})();

let ContributorAccess = (function() {
    let formatToJson = function() {
        return { Name: this.name, Number: this.number, Duration: this.duration };
    };

    return function ContributorAccess(name, number, duration) {
        this.name = name;
        this.number = number;
        this.duration = duration;
        this.formatToJson = formatToJson;
    };
})();

export { ContributorAccessGroup, ContributorAccess };
