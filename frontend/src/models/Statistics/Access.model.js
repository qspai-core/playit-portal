let AccessGroup = (function() {
    let formatToJson = function() {
        return this.accesses.map(e => e.formatToJson());
    };

    return function AccessGroup(total, accesses) {
        this.total = total;
        this.accesses = accesses;
        this.formatToJson = formatToJson;
    };
})();

let Access = (function() {
    let formatToJson = function() {
        return { date: this.date, number: this.number };
    };

    return function Access(date, number) {
        this.date = date;
        this.number = number;
        this.formatToJson = formatToJson;
    };
})();

export { AccessGroup, Access };
