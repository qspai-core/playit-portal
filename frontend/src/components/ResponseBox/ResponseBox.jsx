import React, { useState } from 'react';

import Response from '../Response';
import ResponseForm from '../ResponseForm';

import './ResponseBox.scss'

const ResponseBox = ({responsesArr, complaintId}) => {
    const [ showResponses, setShowResponses ] = useState(false)
    
    const handleClick = event => {
        setShowResponses(!showResponses)
    }

    const getResponses = event => {
        return responsesArr.map((response, index) => {
            return (
                <Response
                    author={response.userId.$id}
                    body={response.text}
                    key={index} />
            );
        });
    }

    const getResponsesTitle = responseCount => {
        if (responseCount === 0) {
            return 'Sem Respostas';
        } else if (responseCount === 1) {
            return "1 Resposta";
        } else {
            return `${responseCount} Respostas`;
        }
    }

    const responses = getResponses();
    let responseNodes;
    let buttonText = 'Mostrar Respostas';

    if (showResponses) {
        buttonText = 'Esconder Respostas';
        responseNodes = <div className="response-list">{responses}</div>;
    }

    return (
        <div className="response-box">
            <h2>Adicione respostas aqui!</h2>
            <ResponseForm className="response-form-wrapper" id={complaintId}/>
            <button id="response-reveal" onClick={handleClick}>
                {buttonText}
            </button>
            <h3>Responses</h3>
            <h4 className="response-count">
                {getResponsesTitle(responses.length)}
            </h4>
            {responseNodes}
        </div>
    );


}

export default ResponseBox;