import React, { useState } from 'react';
import { useAuth } from '../../providers';
import { Link } from 'react-router-dom';

import FormInput from '../FormInput/FormInput';
import CustomButton from '../CustomButton/CustomButton';
import { FacebookFilled } from '@ant-design/icons';
import { notifyError } from '../../utils/notifications';

import './Login.scss';

const Login = () => {
    const [userCredentials, setCredentials] = useState({
        user: '',
        password: '',
    });
    const { user, password } = userCredentials;

    const { signIn } = useAuth();

    const handleSubmit = event => {
        event.preventDefault();

        signIn(user, password, function(message) {
            notifyError(message, null);
        });
    };

    const handleChange = event => {
        const { value, name } = event.target;

        setCredentials({ ...userCredentials, [name]: value });
    };

    return (
        <div className="login">
            <h2>Login</h2>
            <span>Entre com o seu email e password</span>

            <div className="align__facebook__center">
                <div className="facebook__box">
                    <FacebookFilled style={{ color: '#3b5998' }} />
                    <p>Entrar com o Facebook</p>
                </div>
            </div>

            <div className="seperator">
                <hr />
                <p>OU</p>
                <hr />
            </div>
            <form onSubmit={handleSubmit} className="login-form">
                <FormInput
                    name="user"
                    type="user"
                    value={user}
                    handleChange={handleChange}
                    label="Insert Email"
                    required
                />
                <FormInput
                    name="password"
                    type="password"
                    value={password}
                    handleChange={handleChange}
                    label="Insert Password"
                    required
                />
                <div className="login-forgot">
                    <p>
                        <Link to="/forgot">Esqueceu-se da Password ?</Link>
                    </p>
                </div>
                <div className="form-buttons">
                    <CustomButton type="submit"> Login </CustomButton>
                </div>
            </form>
        </div>
    );
};

export default Login;
