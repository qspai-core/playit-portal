import React from 'react';

import './Response.scss';

const Response = ({ author, body }) => {
    return (
        <div className="comment">
            <p className="comment-header">{author}</p>
            <p className="comment-body">- {body}</p>
        </div>
    );
};

export default Response;
