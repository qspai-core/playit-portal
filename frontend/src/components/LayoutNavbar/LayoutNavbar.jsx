import React from 'react';
import PropTypes from 'prop-types';
import Navbar from '../Navbar';

//Providers
import { CurrentHomeProvider } from '../../providers';

const LayoutNavbar = ({ children }) => {
    return (
        <CurrentHomeProvider>
            <div className="app">
                <Navbar />
                <div className="app__content">{children}</div>
            </div>
        </CurrentHomeProvider>
    );
};

LayoutNavbar.propTypes = {
    children: PropTypes.any,
};

export default LayoutNavbar;
