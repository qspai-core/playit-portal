import React, { useState } from 'react';
import { Link } from 'react-router-dom';

// actions
import { useAuth } from '../../providers';

import Burger from './Burger';
import MobileNavbar from './MobileNavbar';
import DesktopProfileMenu from './DesktopProfileMenu';
import profileImg from '../../assets/images/Profile_img.png';
import logo from '../../assets/images/Logo_bg_white.png';
import search from '../../assets/images/search.png';

// the last import should always be the scss
import './Navbar.scss';

//import { NavBar } from './styles';

function Navbar() {
    const { isAuthenticated, userProfile } = useAuth();
    const [isHovering, setHovering] = useState(false);
    const [open, setOpen] = useState(false);

    const handleMouseHover = () => {
        setHovering(!isHovering);
    };

    const handleClickOnMobileItem = () => {
        setOpen(!open);
    };

    const logoLinkUrl = isAuthenticated ? '/dashboard' : '/';
    return (
        <>
            <div className="navbar">
                <Link to={logoLinkUrl}>
                    <img className="navbar__logo" src={logo} alt="Logo play it" />
                </Link>
                <div className="navbar__desktop">
                    <div className="navbar__searchbar">
                        <input className="searchbar_text" placeholder="Procurar jogos"></input>
                        <img className="searchbar_img" src={search} alt="Lupa representativa da pesquisa de jogos" />
                    </div>
                    <ul className="navbar__items">
                        {userProfile.role === 'admin' ? (
                            <>
                                <li>
                                    <Link to="/utilizadores">Utilizadores</Link>
                                </li>
                                <li>
                                    <Link to="/jogosAdmin">Jogos</Link>
                                </li>
                                <li>
                                    <Link to="/denuncias">Denúncias</Link>
                                </li>
                            </>
                        ) : (
                            <>
                                <li>
                                    <Link to="/jogos">Jogos</Link>
                                </li>
                                <li>
                                    <Link to="/estatisticas">Estatisticas</Link>
                                </li>
                                <li>
                                    <Link to="/rankings">Rankings</Link>
                                </li>
                            </>
                        )}
                        {isAuthenticated ? (
                            <div
                                className="profileMenu__wrapper"
                                onMouseEnter={handleMouseHover}
                                onMouseLeave={handleMouseHover}
                            >
                                <div className="navbar__user">
                                    <div className="user_container">
                                        <Link to="/profile">
                                            <p className="user__name">{userProfile.name || 'Nome Exemplo'}</p>
                                        </Link>
                                        <p className="user__role">{userProfile.role}</p>
                                    </div>
                                    <img src={userProfile.avatar || profileImg} alt="Imagem de perfil" />
                                </div>
                                <DesktopProfileMenu visible={isHovering} />
                            </div>
                        ) : (
                            <li>
                                <Link to="/login">Login</Link>
                            </li>
                        )}
                    </ul>
                </div>
                <Burger handleClick={handleClickOnMobileItem} open={open} />
            </div>
            <MobileNavbar
                handleClick={handleClickOnMobileItem}
                open={open}
                userProfile={userProfile}
                isAuthenticated={isAuthenticated}
            />
        </>
    );
}

export default Navbar;
