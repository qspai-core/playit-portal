import React from 'react';

// actions

// the last import should always be the scss
import './Burger.scss';

function Burger({ handleClick, open }) {
  return (
    <div className="burger" open={open} onClick={handleClick}>
      <div className="line"></div>
      <div className="line"></div>
      <div className="line"></div>
    </div>
  );
}

export default Burger;
