import React from 'react';
import { Link } from 'react-router-dom';

// import { Container } from './styles';
import { useAuth } from '../../../providers';
import './MobileNavbar.scss';

const MobileNavbar = ({ handleClick, open, userProfile, isAuthenticated }) => {
    const { signOut } = useAuth();
    function HandleSignOut() {
        signOut();
    }
    return (
        <>
            {open === true && (
                <div className="mobileNavbar">
                    <ul className="navbar__items">
                        {userProfile.role === 'admin' ? (
                            <>
                                <li>
                                    <Link to="/utilizadores" onClick={handleClick}>
                                        Utilizadores
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/jogosAdmin" onClick={handleClick}>
                                        Jogos
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/denuncias" onClick={handleClick}>
                                        Denúncias
                                    </Link>
                                </li>
                            </>
                        ) : (
                            <>
                                <li>
                                    <Link to="/jogos" onClick={handleClick}>
                                        Jogos
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/estatisticas" onClick={handleClick}>
                                        Estatisticas
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/rankings" onClick={handleClick}>
                                        Rankings
                                    </Link>
                                </li>
                            </>
                        )}
                        {isAuthenticated ? (
                            <li>
                                <Link to="/profile">Perfil</Link>
                            </li>
                        ) : (
                            <li>
                                <Link to="/login">Login</Link>
                            </li>
                        )}
                        <li>
                            <Link to="/theme">
                                <span>Temas</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/" onClick={HandleSignOut}>
                                <span>Sair</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            )}
        </>
    );
};

export default MobileNavbar;
