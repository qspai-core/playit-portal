import React from 'react';
import { Link } from 'react-router-dom';

// Providers
import { useAuth } from '../../../providers';

// actions

// the last import should always be the scss

//import { NavBar } from './styles';

import './DesktopProfileMenu.scss';

function DesktopProfileMenu({ visible }) {
    const { signOut } = useAuth();
    function HandleSignOut() {
        signOut();
    }
    return (
        <ul className={visible ? 'profile__menu show' : 'profile__menu hide'}>
            <Link to="/profile">
                <span>Definições de Perfil</span>
            </Link>
            <Link to="/theme">
                <span>Temas</span>
            </Link>
            <Link to="/" onClick={HandleSignOut}>
                <span>Sair</span>
            </Link>
        </ul>
    );
}

DesktopProfileMenu.propTypes = {};

export default DesktopProfileMenu;
