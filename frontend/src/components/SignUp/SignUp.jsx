import React, { useState } from 'react';

import FormInput from '../FormInput/FormInput';
import CustomButton from '../CustomButton';
import { DatePicker } from 'antd';

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { request } from '../../utils/requests';

import 'antd/dist/antd.css';

// scss
import './SignUp.scss';
import { Redirect } from 'react-router-dom';

const SignUp = props => {
    const [newUserCredentials, setNewUserCredentials] = useState({
        email: '',
        password: '',
        confirmPassword: '',
        name: '',
        birth_date: '',
        roleId: '',
    });

    const { email, password, confirmPassword, name, birth_date, roleId } = newUserCredentials;

    const handleSubmit = event => {
        event.preventDefault();

        if (password !== confirmPassword) {
            alert("Password don't match");
            return;
        }

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password,
                confirmPassword: confirmPassword,
                name: name,
                birth_date: birth_date,
                avatar: '',
                roleId: roleId,
            }),
        };

        const url = `${process.env.REACT_APP_BACKEND_URL}/user`;
        request(url, requestOptions, 'Signup Success', function(res) {
            const { from = '/' } = props.location.state || {};
            return <Redirect to={from} />;
        });
    };

    const handleChange = event => {
        const { value, name } = event.target;

        setNewUserCredentials({ ...newUserCredentials, [name]: value });
    };

    const handleDateChange = (date, dateString) => {
        var aux = newUserCredentials;
        aux.birth_date = dateString;
        setNewUserCredentials(aux);
    };

    return (
        <div className="signup__page">
            <h2>Registar</h2>
            <span>Registe-se com o seu email e password</span>

            <form onSubmit={handleSubmit} className="signup__form">
                <FormInput name="name" type="name" value={name} handleChange={handleChange} label="Nome" required />
                <FormInput name="email" type="email" value={email} handleChange={handleChange} label="Email" required />
                <div className="date__picker">
                    <label>Data de nascimento:</label>
                    <DatePicker onChange={(date, dateString) => handleDateChange(date, dateString)} />
                </div>
                <FormInput name="roleId" type="roleId" value={roleId} handleChange={handleChange} label="Role" />
                <FormInput
                    name="password"
                    type="password"
                    value={password}
                    handleChange={handleChange}
                    label="Password"
                    required
                />
                <FormInput
                    name="confirmPassword"
                    type="password"
                    value={confirmPassword}
                    handleChange={handleChange}
                    label="Confirmar Password"
                    required
                />
                <div className="form-buttons">
                    <CustomButton type="submit"> Registar </CustomButton>
                </div>
            </form>
            <ToastContainer />
        </div>
    );
};
export default SignUp;
