import React, { useState } from 'react';

import { useAuth } from '../../providers';

import './ResponseForm.scss';

const ResponseForm = ({ id }) => {
    /* const [inputs, setInputs] = useState({
        author: '',
        response: ''
    }) */

    const [response, setResponse] = useState('');
    const { userProfile } = useAuth();
    // const { author, response } = inputs;

    const addComment = event => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                userId: userProfile._id,
                response: response,
            }),
        };

        fetch(`${process.env.REACT_APP_BACKEND_URL}/complaint/response/${id}`, requestOptions)
            .then(r => r.json())
            .then(function(res) {
                return window.location.reload(false);
            });
    };

    const handleSubmit = event => {
        event.preventDefault();

        //  setInputs(author, res)
        addComment(response);
        // console.log(inputs);
    };

    const handleChangeResponse = event => {
        setResponse(event.target.value);
    };

    return (
        <form className="response-form" onSubmit={handleSubmit}>
            <textarea type="text" placeholder="Resposta" onChange={handleChangeResponse} />

            <input type="submit" value="Adicionar Comentário" />
        </form>
    );
};

export default ResponseForm;
