import React from 'react';
import Router from './router';
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return <>
    <ToastContainer autoClose={8000} />
    <Router />
  </>
}

export default App;
