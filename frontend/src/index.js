import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

// fetch polyfill
import 'whatwg-fetch';

import * as serviceWorker from './serviceWorker';

import './assets/_shared.scss';

import App from './App';

import { CurrentAuthProvider } from './providers';

if (process.env.NODE_ENV !== 'production') {
  import('react-axe').then(axe => {
    axe.default(React, ReactDOM, 1000);
    ReactDOM.render(
      <CurrentAuthProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </CurrentAuthProvider>,
      document.getElementById('root'),
    );
  });
} else {
  ReactDOM.render(
    <CurrentAuthProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </CurrentAuthProvider>,

    document.getElementById('root'),
  );
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
