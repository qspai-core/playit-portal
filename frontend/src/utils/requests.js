
import { notifySuccess, notifyError } from "./notifications";
import { handleErrors } from "./errorHandlers";

export function header() {
    return {
      "Content-Type": "application/json"
    };
  }
  
export async function request(url, options, successMessage, successHandler){
    fetch(url, options)
    .then(handleErrors)
    .then((res) => res)
    .then(function(res) {
        notifySuccess(successMessage);
        if(successHandler){
            successHandler(res);
        }       
    })
    .catch((error) => {       
        notifyError(error);
      });
}  