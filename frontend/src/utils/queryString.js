export const getQueryStringFromObject = (obj) => {
    return Object.entries(obj)
      .filter(([, value]) => value !== null && value !== undefined)
      .map(([key, value]) => mapToQueryStringParam(key, value))
      .join("&");
  };
  
const mapToQueryStringParam = (key, value) => {
if (!Array.isArray(value)) {
    return `${key}=${value}`;
}

return value.map((val) => `${key}=${val}`).join("&");
};