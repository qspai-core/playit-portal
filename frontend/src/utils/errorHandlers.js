export async function handleErrors(response) {
    if (!response.ok) {
        let responseText = await response.text();
        const data = responseText && JSON.parse(responseText);
        return Promise.reject(getError(data, response));
    }

    return Promise.resolve(response);
}

function getError(data, response) {
    if(!data.errors){
        return data["error"] && data["error"].message || response.statusText ;
    }

    if(!data.errors.error || !data.errors.error.errors){
        return "Occorreu algum problema, tenta mais tarde."
    }

    let str = "";
    data.errors.error.errors.forEach(element => {
        str += element.msg + "\n";
    });

    return str;
}
