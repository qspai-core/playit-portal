import { toast } from "react-toastify";
import { css } from "glamor";

export function notifySuccess(messageNotif) {
  toast.success(messageNotif, {
    className: css({
      background: "#43a047 !important",
      borderRadius: "5px !important",
      boxShadow:
        "0px 5px 5px -3px rgba(80,80,80, 0.2), 0px 8px 10px 1px rgba(80,80,80, 0.14), 0px 3px 14px 2px rgba(80,80,80, 0.12) !important",
    }),
  });
}

export function notifyError(messageNotif, id) {
  toast.error(messageNotif, {
    className: css({
      background: "#b20000 !important",
      borderRadius: "5px !important",
      "& ul": {
        listStyleType: "circle",
        padding: "0 0 0 30px",
      },
      boxShadow:
        "0px 5px 5px -3px rgba(80,80,80, 0.2), 0px 8px 10px 1px rgba(80,80,80, 0.14), 0px 3px 14px 2px rgba(80,80,80, 0.12) !important",
    }),
    toastId: id
  });
}

export function notifyInformation(messageNotif) {
  toast.error(messageNotif, {
    className: css({
      background: "#cce5ff !important",
      borderRadius: "5px !important",
      color: "#004085",
      "& ul": {
        listStyleType: "circle",
        padding: "0 0 0 30px",
      },
      boxShadow:
        "0px 5px 5px -3px rgba(80,80,80, 0.2), 0px 8px 10px 1px rgba(80,80,80, 0.14), 0px 3px 14px 2px rgba(80,80,80, 0.12) !important",
      whiteSpace: "pre-line",
    }),
  });
}

export function notifyWarning(messageNotif) {
  toast.error(messageNotif, {
    className: css({
      background: "#fff3cd !important",
      borderRadius: "5px !important",
      color: "#856404",
      "& ul": {
        listStyleType: "circle",
        padding: "0 0 0 30px",
      },
      boxShadow:
        "0px 5px 5px -3px rgba(80,80,80, 0.2), 0px 8px 10px 1px rgba(80,80,80, 0.14), 0px 3px 14px 2px rgba(80,80,80, 0.12) !important",
    }),
  });
}
