import Home from './pages/Home';
import LoginAndSignup from './pages/LoginAndSignup';
import Logout from './pages/Logout';
import Ranking from './pages/Ranking';
import Profile from './pages/Profile';
import Statistics from './pages/Statistics';
import SomePrivatePage from './pages/SomePrivatePage';
import LayoutNavbar from './components/LayoutNavbar';
import PasswordRecover from './pages/PasswordRecover';
import ForgotPassword from './pages/ForgotPassword';
import ThemeSelection from './pages/ThemeSelection'
import ConfirmEmail from './pages/ConfirmEmail/ConfirmEmail';
import Users from './pages/Admin/Users';
import Games from './pages/Admin/Games';
import Complaints from './pages/Admin/Complaints'
import Complaint from './pages/Admin/Complaint'
import Dashboard from "./pages/Dashboard";

const routes = [
    { path: '/', exact: true, layout: LayoutNavbar, component: Home },
    {
        path: '/login',
        exact: true,
        component: LoginAndSignup,
        layout: LayoutNavbar,
    },
    {
        path: '/logout',
        exact: true,
        component: Logout,
    },
    {
        path: '/private',
        isPrivate: true,
        layout: LayoutNavbar,
        component: SomePrivatePage,
    },
    {
        path: '/profile',
        exact: true,
        isPrivate: true,
        layout: LayoutNavbar,
        component: Profile,
    },
    {
        path: '/rankings',
        exact: true,
        layout: LayoutNavbar,
        component: Ranking,
    },
    {
        path: '/estatisticas',
        exact: true,
        layout: LayoutNavbar,
        component: Statistics,
    },
    {
        path: '/dashboard',
        exact: true,
        layout: LayoutNavbar,
        component: Dashboard,
    },
    {
        path: '/forgot',
        exact: true,
        layout: LayoutNavbar,
        component: ForgotPassword,
    },
    {
        path: '/recover',
        exact: true,
        layout: LayoutNavbar,
        component: PasswordRecover,
    },
    {
        path: '/theme',
        exact: true,
        layout: LayoutNavbar,
        component: ThemeSelection,
    },
    {
        path: '/activate',
        exact: true,
        layout: LayoutNavbar,
        component: ConfirmEmail,
    },
    {
        path: '/activate',
        exact: true,
        layout: LayoutNavbar,
        component: ConfirmEmail,
    },
    //ADMIN
    {
        path: '/utilizadores',
        exact: true,
        layout: LayoutNavbar,
        component: Users,
    },
    {
        path: '/jogosAdmin',
        exact: true,
        layout: LayoutNavbar,
        component: Games
    },
    {
        path: '/denuncias',
        exact: true,
        layout: LayoutNavbar,
        component: Complaints
    },
    {
        path: '/denuncia/:id',
        exact: true,
        layout: LayoutNavbar,
        component: Complaint
    }
];

export default routes;
