import React, { useState, createContext, useContext } from 'react';

const CurrentHomeContext = createContext({
  hello: '',
  changeHelloWord: () => {},
});

export const CurrentHomeProvider = ({ children }) => {
  const [hello, setHello] = useState('');

  const changeHelloWord = () => {
    const helloArray = ['Hey!', 'Hello!', 'Oi!', 'Boas!', 'Yarr!', 'Hiho!'];
    const randomHelloArrayValue = helloArray[Math.floor(Math.random() * helloArray.length)];

    setHello(randomHelloArrayValue);
  };

  return <CurrentHomeContext.Provider value={{ hello, changeHelloWord }}>{children}</CurrentHomeContext.Provider>;
};

export const useHello = () => useContext(CurrentHomeContext);
