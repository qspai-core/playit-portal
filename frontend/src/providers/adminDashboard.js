import React, { useState, createContext, useContext, useEffect } from 'react';
import { header, request } from '../utils/requests';
import { AccessGroup, Access, ContributorAccess, ContributorAccessGroup } from '../models';
import { dateToISOString } from '../utils/dateFormatters';

const CurrentAdminDashboardContext = createContext({
    isLoading: false,
    getAllAccess: () => {},
    allAccess: null,
});

const CurrentAdminDashboardProvider = ({ children }) => {
    const [isLoading, setIsLoading] = useState(true);
    const [allAccess, setAllAccess] = useState(null);
    const [contributorsAccess, setContributorsAccess] = useState(null);

    const getAllAccess = (from, to) => {
        const requestOptions = {
            method: 'GET',
            headers: header(),
        };

        const fromDate = dateToISOString(new Date(from));
        const toDate = dateToISOString(new Date(to));

        fetch(`${process.env.REACT_APP_BACKEND_URL}/statistics/user?from=${fromDate}&to=${toDate}`, requestOptions)
            .then(response => response.json())
            .then(function(e) {
                setIsLoading(false);
                if (e.success) {
                    setAllAccess(
                        new AccessGroup(
                            e.data.total,
                            e.data.grouped.map(e => new Access(e.date, e.count)),
                        ),
                    );
                } else {
                    setAllAccess(null);
                }
            });
    };

    const getContributorsAccess = (from, to) => {
        const requestOptions = {
            method: 'GET',
            headers: header(),
        };

        const fromDate = dateToISOString(new Date(from));
        const toDate = dateToISOString(new Date(to));

        fetch(
            `${process.env.REACT_APP_BACKEND_URL}/statistics/game/contributor?from=${fromDate}&to=${toDate}`,
            requestOptions,
        )
            .then(response => response.json())
            .then(function(e) {
                setIsLoading(false);
                if (e.success) {
                    setContributorsAccess(
                        new ContributorAccessGroup(
                            e.data.contributor.map(e => new ContributorAccess(e.name, e.count, e.duration)),
                        ),
                    );
                } else {
                    setContributorsAccess(null);
                }
            });
    };

    return (
        <CurrentAdminDashboardContext.Provider
            value={{ getAllAccess, getContributorsAccess, isLoading, allAccess, contributorsAccess }}
        >
            {children}
        </CurrentAdminDashboardContext.Provider>
    );
};

const useAdminDashboard = () => useContext(CurrentAdminDashboardContext);
export { CurrentAdminDashboardProvider, useAdminDashboard };
