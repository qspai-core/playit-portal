import React, { useState, useEffect } from 'react';

import './Complaints.scss';

function Complaints() {
    const [complaints, setComplaints] = useState([]);
    const skip = 0;

    const fetchData = () => {
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        };

        fetch(`${process.env.REACT_APP_BACKEND_URL}/complaint/all/${skip}/5`, requestOptions)
            .then(response => response.json())
            .then(function(res) {
                setComplaints(res.data);
            });
    };

    useEffect(() => {
        fetchData();
    }, []);

    const url = id => {
        return `/denuncia/${id}`;
    };

    return (
        <div className="complaints">
            <h2>Denuncias</h2>
            <>
                {complaints.length != 0
                    ? complaints.map(c => {
                          if (!c.isArchived) {
                              return (
                                  <>
                                      <label>Titulo:</label>
                                      <p>{c.title}</p>
                                      <label>Criado por:</label>
                                      <p>{c.createdBy.$id}</p>
                                      <label>Descrição:</label>
                                      <p>{c.description}</p>
                                      <a id="complaint-a-id" href={url(c._id)}>
                                          Mais detalhes aqui!
                                      </a>
                                  </>
                              );
                          }
                      })
                    : 'Loading...'}
            </>
        </div>
    );
}

export default Complaints;
