import React, { useState, useEffect } from 'react';

import { useAuth } from '../../../providers';

import './Complaint.scss';

import ResponseBox from '../../../components/ResponseBox';

function Complaint({ location }) {
    const id = location.pathname.split('/')[2];
    const [complaint, setComplaint] = useState({});
    const [isReady, setIsReady] = useState(false);

    const { userProfile } = useAuth();

    const fetchData = () => {
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
        };

        fetch(`${process.env.REACT_APP_BACKEND_URL}/complaint/${id}`, requestOptions)
            .then(response => response.json())
            .then(function(res) {
                setComplaint(res.data);
                setIsReady(true);
            });
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleArchive = bool => {
        const requistArchive = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                archived: bool,
                userId: userProfile._id,
            }),
        };
        fetch(`${process.env.REACT_APP_BACKEND_URL}/complaint/archived/${id}`, requistArchive)
            .then(response => response.json())
            .then(function(res) {
                return window.location.reload(false);
            });
    };

    return (
        <div className="complaint-wrapper">
            <h2>Denuncia</h2>

            {isReady === true ? (
                <>
                    <h2>Titulo:</h2>
                    <p>{complaint.title}</p>
                    <h2>Criado por:</h2>
                    <p>{complaint.createdBy.$id}</p>
                    <h2>Descrição:</h2>
                    <p>{complaint.description}</p>
                    <h2>Está arquivado?</h2>
                    {complaint.isArchived ? (
                        <div className="archived-wrapper">
                            <p>Sim</p> <button onClick={() => handleArchive(false)}>Desarquivar</button>{' '}
                        </div>
                    ) : (
                        <div className="archived-wrapper">
                            <p>Não</p> <button onClick={() => handleArchive(true)}>Arquivar</button>{' '}
                        </div>
                    )}

                    <ResponseBox responsesArr={complaint.responses} complaintId={complaint._id} />
                </>
            ) : (
                'Loading...'
            )}
        </div>
    );
}

export default Complaint;
