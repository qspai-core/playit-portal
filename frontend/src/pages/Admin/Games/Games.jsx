import React, { useState, useEffect } from 'react';

import Draggable from 'react-draggable';
import Modal from 'react-modal';

import profileImg from '../../../assets/images/Profile_img.png';

import './Games.scss';

const { innerWidth: width } = window;

const Games = () => {
    const [games, setGames] = useState([]);
    const [selectedGame, setGame] = useState({});
    const [isOpen, setOpen] = useState(false);
    const [contributorName, setContributorName] = useState('');

    const [currentPage, setCurrentPage] = useState(1);
    const [lastPage, setLastPage] = useState(0);
    const [nextPage, setNextPage] = useState(2);

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    useEffect(() => {
        const fetchData = () => {
            if (width < 768) {
                const skip = (currentPage - 1) * 2;

                fetch(`${process.env.REACT_APP_BACKEND_URL}/game/all/${skip}/2`, requestOptions)
                    .then(response => response.json())
                    .then(function(res) {
                        setGames(res.data);
                    });
            } else {
                const skip = (currentPage - 1) * 12;

                fetch(`${process.env.REACT_APP_BACKEND_URL}/game/all/${skip}/12`, requestOptions)
                    .then(response => response.json())
                    .then(function(res) {
                        setGames(res.data);
                    });
            }
        };

        fetchData();
    }, [currentPage]);

    const requestActivate = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            active: true,
        }),
    };
    const handleActivate = id => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}/game/active/${id}`, requestActivate);
        window.location.reload(false);
    };

    const requestDesactivate = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            active: false,
        }),
    };

    const handleDesactivate = id => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}/game/active/${id}`, requestDesactivate);
        window.location.reload(false);
    };

    const handleStop = (id, d) => {
        //Para web responsive (versão laptop)
        if (width >= 768) {
            //Para desativar
            if (d.y >= -60 && d.x >= 391 && d.y <= 246 && d.x <= 1200) {
                return handleDesactivate(id);
            }
            //Para ativar
            if (d.y >= -60 && d.x >= -1200 && d.y <= 246 && d.x <= 391) {
                return handleActivate(id);
            }

            return window.location.reload(false);
        } else {
            //se maior= que y=-51 e x >= 165 e menor=< y=241 e x<= 265 fica inativo
            if (d.y >= -51 && d.x >= 165 && d.y <= 241 && d.x <= 265) {
                return handleDesactivate(id);
            }
            //Para ativar
            if (d.y >= -51 && d.x >= -265 && d.y <= 241 && d.x <= -165) {
                return handleActivate(id);
            }

            return window.location.reload(false);
        }
    };

    const openModal = game => {
        Modal.setAppElement('#draggable-selected-wrapper');
        setGame(game);
        fetchOwnerName(game.ownerId.$id);
        setOpen(true);
    };

    const closeModal = () => {
        setOpen(false);
    };

    const fetchOwnerName = id => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}/user/${id}`, requestOptions)
            .then(response => response.json())
            .then(function(res) {
                setContributorName(res.data.name);
            });
    };

    const handleDrag = id => {
        document.getElementById(id).style.visibility = 'hidden';
    };

    const handleNextPage = () => {
        let next = currentPage;
        next++;
        setLastPage(currentPage);
        setCurrentPage(next);
        next++;
        setNextPage(next);
        //window.location.reload(false);
    };

    const previousPage = () => {
        let previous = currentPage;
        previous--;
        setNextPage(currentPage);
        setCurrentPage(previous);
        previous--;
        setLastPage(previous);
        // window.location.reload(false);
    };

    return (
        <>
            <h1 id="draggable-title-games">Jogos</h1>
            <div className="games-draggable-wrapper" id="games-draggable-wrapper">
                <div className="left-wrapper">
                    <h2>Activos</h2>
                    <div>
                        {games.map(g => {
                            if (g.isActive === true)
                                return (
                                    <div
                                        key={g._id}
                                        className="draggable-selected-wrapper"
                                        id="draggable-selected-wrapper"
                                    >
                                        <Draggable
                                            onStop={(e, d) => handleStop(g._id, d)}
                                            onDrag={() => handleDrag(g._id)}
                                        >
                                            <div className="game-div-wrapper">
                                                <img
                                                    value={g._id}
                                                    src={g.image ? g.image : profileImg}
                                                    alt="Imagem de perfil"
                                                />
                                                <p>{g.name}</p>
                                            </div>
                                        </Draggable>
                                        <button id={g._id} value={g} onClick={() => openModal(g)}>
                                            Detalhes
                                        </button>
                                        {isOpen === true && (
                                            <Modal
                                                isOpen={isOpen}
                                                onRequestClose={closeModal}
                                                className="modal-style"
                                                aria={{
                                                    labelledby: 'Detalhes do Jogo',
                                                    describedby: 'Informação sobre o jogo',
                                                }}
                                            >
                                                <div className="modal-header">
                                                    <button onClick={closeModal}>X</button>
                                                    <h1>{selectedGame.name}</h1>
                                                    <img
                                                        src={selectedGame.image || profileImg}
                                                        alt="Imagem de perfil"
                                                    />
                                                </div>
                                                <div className="modal-info-container">
                                                    <div className="modal-game-info">
                                                        <h2>Contribuidor:</h2>
                                                        <p>{contributorName}</p>
                                                    </div>

                                                    <div className="modal-game-info">
                                                        <h2>Activo:</h2>
                                                        <p>{selectedGame.isActive ? 'Sim' : 'Não'}</p>
                                                    </div>
                                                </div>
                                            </Modal>
                                        )}
                                    </div>
                                );
                        })}
                    </div>
                </div>
                <div className="right-wrapper">
                    <h2>Inativos</h2>
                    <div>
                        {games.map(g => {
                            if (g.isActive === false)
                                return (
                                    <div
                                        key={g._id}
                                        className="draggable-selected-wrapper"
                                        id="draggable-selected-wrapper"
                                    >
                                        <Draggable
                                            onStop={(e, d) => handleStop(g._id, d)}
                                            onDrag={() => handleDrag(g._id)}
                                        >
                                            <div className="game-div-wrapper">
                                                <img
                                                    value={g._id}
                                                    src={g.image ? g.image : profileImg}
                                                    alt="Imagem de perfil"
                                                />
                                                <p>{g.name}</p>
                                            </div>
                                        </Draggable>
                                        <button id={g._id} value={g} onClick={() => openModal(g)}>
                                            Detalhes
                                        </button>
                                        {isOpen === true && (
                                            <Modal
                                                isOpen={isOpen}
                                                onRequestClose={closeModal}
                                                className="modal-style"
                                                aria={{
                                                    labelledby: 'Detalhes do Jogo',
                                                    describedby: 'Informação sobre o jogo',
                                                }}
                                            >
                                                <div className="modal-header">
                                                    <button onClick={closeModal}>X</button>
                                                    <h1>{selectedGame.name}</h1>
                                                    <img
                                                        src={selectedGame.image || profileImg}
                                                        alt="Imagem de perfil"
                                                    />
                                                </div>
                                                <div className="modal-info-container">
                                                    <div className="modal-game-info">
                                                        <h2>Contribuidor:</h2>
                                                        <p>{contributorName}</p>
                                                    </div>

                                                    <div className="modal-game-info">
                                                        <h2>Activo:</h2>
                                                        <p>{selectedGame.isActive ? 'Sim' : 'Não'}</p>
                                                    </div>
                                                </div>
                                            </Modal>
                                        )}
                                    </div>
                                );
                        })}
                    </div>
                </div>
            </div>
            <div className="pagination-wrapper">
                {currentPage !== 1 && <button onClick={() => previousPage()}>{lastPage}</button>}
                <button>{currentPage}</button>
                <button onClick={() => handleNextPage()}>{nextPage}</button>
            </div>
        </>
    );
};

export default Games;
