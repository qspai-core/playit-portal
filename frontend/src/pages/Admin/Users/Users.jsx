import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';

import profileImg from '../../../assets/images/Profile_img.png';

import './Users.scss';

const Users = () => {
    const [users, setUsers] = useState([]);
    const [isOpen, setOpen] = useState(false);
    const [userSelected, setUserSelected] = useState({});
    const [filter, setFilter] = useState('users');

    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };

    useEffect(() => {
        const fetchData = () => {
            fetch(`${process.env.REACT_APP_BACKEND_URL}/user/all/0/20`, requestOptions)
                .then(response => response.json())
                .then(function(res) {
                    setUsers(res.data);
                });
        };

        fetchData();
    }, []);

    //QUANDO O FILTER MUDA O USERS STATE MUDA PARA A COLEÇÃO DOS CONTRIBUTORS
    useEffect(() => {
        switch (filter) {
            case 'users':
                const fetchUsers = () => {
                    fetch(`${process.env.REACT_APP_BACKEND_URL}/user/all/0/20`, requestOptions)
                        .then(response => response.json())
                        .then(function(res) {
                            setUsers(res.data);
                        });
                };

                fetchUsers();
                break;
            case 'contributors':
                const fetchContributors = () => {
                    fetch(`${process.env.REACT_APP_BACKEND_URL}/contributors/all/0/20`, requestOptions)
                        .then(response => response.json())
                        .then(function(res) {
                            setUsers(res.data);
                        });
                };

                fetchContributors();
                break;
            case 'players':
                const fetchPlayers = () => {
                    fetch(`${process.env.REACT_APP_BACKEND_URL}/players/all/0/20`, requestOptions)
                        .then(response => response.json())
                        .then(function(res) {
                            setUsers(res.data);
                        });
                };

                fetchPlayers();
                break;
            default:
                const fetchData = () => {
                    fetch(`${process.env.REACT_APP_BACKEND_URL}/user/all/0/20`, requestOptions)
                        .then(response => response.json())
                        .then(function(res) {
                            setUsers(res.data);
                        });
                };

                fetchData();
        }
    }, [filter, userSelected]);

    useEffect(() => {
        if (userSelected.isActive === true) {
            const requestDesactivate = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    active: false,
                }),
            };
            const fetchData = () => {
                fetch(`${process.env.REACT_APP_BACKEND_URL}/user/active/${userSelected._id}`, requestDesactivate);
            };
            fetchData();
        }
        if (userSelected.isActive === false) {
            const requestActivate = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    active: true,
                }),
            };
            const fetchData = () => {
                fetch(`${process.env.REACT_APP_BACKEND_URL}/user/active/${userSelected._id}`, requestActivate);
            };
            fetchData();
        }
    }, [userSelected]);

    const openModal = user => {
        Modal.setAppElement('#buttons-wrap');
        setUserSelected(user);
        setOpen(true);
    };

    const closeModal = () => {
        setOpen(false);
    };

    const handleActivateDesactivate = user => {
        setUserSelected(user);
    };

    const handleFilter = filter => {
        setFilter(filter);
    };

    return (
        <div className="users-wrapper">
            <h1>Utilizadores</h1>
            <div className="nav-wrapper">
                <button onClick={() => handleFilter('users')} className={filter === 'users' ? 'bold' : ''}>
                    Todos
                </button>
                <button onClick={() => handleFilter('players')} className={filter === 'players' ? 'bold' : ''}>
                    Jogadores
                </button>
                <button
                    onClick={() => handleFilter('contributors')}
                    className={filter === 'contributors' ? 'bold' : ''}
                >
                    Contribuidores
                </button>
            </div>

            <div className="users-info-wrapper">
                <div className="users-info-titles">
                    <h2>Nome</h2>
                    <h2 className="show-web">Email</h2>
                    <h2 className="show-web">Verificado</h2>
                    <h2>Ativo</h2>
                    <h2></h2>
                </div>

                {users.map(user => (
                    <div key={user._id} className="user-info">
                        <p>{user.name.split(' ')[0]}</p>
                        <p className="show-web">{user.email}</p>
                        <p className="show-web">{user.emailConfirmed ? 'Sim' : 'Não'}</p>
                        <p>{user.isActive ? 'Sim' : 'Não'}</p>
                        <div className="buttons-wrapper" id="buttons-wrap">
                            <button value={user} onClick={() => openModal(user)}>
                                Perfil
                            </button>

                            {isOpen === true && (
                                <Modal
                                    isOpen={isOpen}
                                    onRequestClose={closeModal}
                                    className="modal-style"
                                    aria={{
                                        labelledby: 'Perfil do Utilizador',
                                        describedby: 'Informação sobre o perfil de utilizador',
                                    }}
                                >
                                    <div className="modal-header">
                                        <button onClick={closeModal}>X</button>
                                        <h1>{userSelected.name}</h1>
                                        <img src={userSelected.avatar || profileImg} alt="Imagem de perfil" />
                                    </div>
                                    <div className="modal-info-container">
                                        <div className="modal-user-info">
                                            <h2>Email:</h2>
                                            <p>{userSelected.email}</p>
                                        </div>
                                        <div className="modal-user-info">
                                            <h2>Aniversário:</h2>
                                            <p>{userSelected.birthDate}</p>
                                        </div>
                                        <div className="modal-user-info">
                                            <h2>Email Confirmado:</h2>
                                            <p>{userSelected.emailConfirmed ? 'Sim' : 'Não'}</p>
                                        </div>
                                        <div className="modal-user-info">
                                            <h2>Activo:</h2>
                                            <p>{userSelected.isActive ? 'Sim' : 'Não'}</p>
                                        </div>
                                    </div>
                                </Modal>
                            )}

                            {user.isActive === true && (
                                <button onClick={() => handleActivateDesactivate(user)} className="desactivate-button">
                                    Desativar
                                </button>
                            )}
                            {user.isActive === false && (
                                <button onClick={() => handleActivateDesactivate(user)} className="activate-button">
                                    Ativar
                                </button>
                            )}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Users;
