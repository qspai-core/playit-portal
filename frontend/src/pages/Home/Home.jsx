import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

// Providers
//simport { useHello } from '../../providers';
import jogoExemplo from '../../assets/images/Jogo_exemplo.png';
import Topplayers_Arrow from '../../assets/images/Topplayers_Arrow.png';
import Podium from '../../assets/images/Podium.png';
import BestGames_fire from '../../assets/images/BestGames_Fire.png';
import Contributor_Home from '../../assets/images/Contributor_Home.png';
// scss
import './Home.scss';
//import { HomePage } from './styles';

function Home(/* props */) {
    //const { hello, changeHelloWord } = useHello();
    const [topGames, setTopGames] = useState([]);

    useEffect(() => {
        setUpTopGames();
    }, []);

    const setUpTopGames = () => {
        let games = [];
        for (let index = 0; index < 3; index++) {
            const game = {
                id: index,
                img: jogoExemplo,
                alt: 'imagem exemplo',
                name: 'Jogo exemplo',
            };
            games.push(game);
        }
        setTopGames(games);
    };

    return (
        <div className="home">
            {/* <h2 className="home__hello">{hello}</h2> */}
            <div className="home__bestGames">
                <div className="bestGames__title">
                    <h2>Melhores jogos</h2>
                    <img src={BestGames_fire} alt="" />
                </div>
                <div className="bestGames_games">
                    {topGames.map(game => {
                        return (
                            <div key={game.id} className="game__info">
                                <img src={game.img} alt={game.alt} />
                                <p>{game.name}</p>
                            </div>
                        );
                    })}
                </div>
            </div>
            <div className="home__topPlayers">
                <div className="topPlayers__title">
                    <h2>Top Jogadores</h2>
                    <img src={Topplayers_Arrow} alt="" />
                </div>
                <div className="topPlayers__Ranking">
                    <img className="podium__img" src={Podium} alt="" />
                </div>
            </div>
            <div className="home_becomeContributor">
                <h2 className="becomeContributor__title">Torne-se um contribuidor</h2>
                <div className="becomeContributor_info">
                    <img src={Contributor_Home} alt="" />
                    <div className="info__advantages">
                        <ul>
                            <li>Submeta o seu jogo</li>
                            <li>Visualize as estatisticas</li>
                            <li>Ganhe mais jogadores</li>
                        </ul>
                        <Link to="/contributor">Comece já</Link>
                    </div>
                </div>
            </div>
        </div>
    );
}

Home.propTypes = {
    hello: PropTypes.string,
    setHomeHello: PropTypes.func,
};

export default Home;