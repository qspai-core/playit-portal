import React, { useState } from 'react';
import { Select } from 'antd';

import './ThemeSelection.scss';

function ThemeSelection(props) {
    const themes = ['deafult', 'dark'];
    const { Option } = Select;
    const [user, setUser] = useState({
        theme: 'default',
    });

    const changeTheme = theme => {
        user.theme = theme;

        document.documentElement.className = '';
        document.documentElement.classList.add(`theme-${user.theme}`);
    };

    return (
        <div className="theme__page">
            <h1 className="theme__title">Themes</h1>
            <p>Choose theme</p>
            <Select defaultValue={user.theme} onChange={changeTheme}>
                {themes.map((theme, index) => (
                    <Option value={theme} key={index}>
                        {theme}
                    </Option>
                ))}
            </Select>
        </div>
    );
}

export default ThemeSelection;
