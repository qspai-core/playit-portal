import React, { useState } from 'react';

import './PasswordRecover.scss';

import FormInput from '../../components/FormInput/FormInput';
import CustomButton from '../../components/CustomButton';

import { request } from '../../utils/requests';
import { Redirect } from 'react-router-dom';

const PasswordRecover = ({ location }) => {
    const [userPassword, setUserPassword] = useState({
        password: '',
        confirmPassword: '',
        userId: location.search.split('?')[1],
    });
    const { password, confirmPassword, userId } = userPassword;

    console.log(userId);
    if (!userId) {
        return (
            <div>
                <h2>Error</h2>
                <span>Page not found!</span>
            </div>
        );
    }

    const handleSubmit = event => {
        event.preventDefault();

        if (password !== confirmPassword) {
            alert("Password don't match");
            return;
        }

        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                password: password,
            }),
        };
        const url = `${process.env.REACT_APP_BACKEND_URL}/user/password/${userId}`;
        request(url, requestOptions, 'Password Changed', function(res) {
            const { from = '/' } = location.state || {};
            return <Redirect to={from} />;
        });
    };

    const handleChange = event => {
        const { value, name } = event.target;

        setUserPassword({ ...userPassword, [name]: value });
    };

    return (
        <div className="recover-page">
            <h2>Password Recovery</h2>
            <span>Enter your new Password</span>
            <form onSubmit={handleSubmit} className="recover-form">
                <FormInput
                    name="password"
                    type="password"
                    value={password}
                    handleChange={handleChange}
                    label="Insert Password"
                    required
                />
                <FormInput
                    name="confirmPassword"
                    type="password"
                    value={confirmPassword}
                    handleChange={handleChange}
                    label="Confirm Password"
                    required
                />
                <CustomButton type="submit">Change Password</CustomButton>
            </form>
        </div>
    );
};

export default PasswordRecover;
