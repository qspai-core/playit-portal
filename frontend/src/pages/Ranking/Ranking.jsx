/**
 * @author Tomás Santos
 * @description
 * Página dos rankings, desenhada como está no desenho de alto nível
 */

import React, { useState } from 'react';
import './Ranking.scss';
import { dummyDataMatch } from './data.json';

const Ranking = props => {
    const [mode, setMode] = useState('single-player');

    const orderSingle = () => {
        document.getElementById('multiButton').style.fontWeight = 'normal';
        document.getElementById('singleButton').style.fontWeight = 'bold';
        setMode('single-player');
    };
    const orderMulti = () => {
        document.getElementById('singleButton').style.fontWeight = 'normal';
        document.getElementById('multiButton').style.fontWeight = 'bold';
        setMode('multi-player');
    };
    return (
        <div className="title">
            <h1>Ranking</h1>
            <button className="modeButton" id="singleButton" onClick={orderSingle}>
                Jogo singular
            </button>
            <button className="modeButton" id="multiButton" onClick={orderMulti}>
                Multi-jogador
            </button>
            <div className="ranking-container">
                <div className="difficulty-container">
                    <h2>Fácil</h2>
                    {dummyDataMatch
                        .filter(e => e.difficulty === 'easy' && e.result === 'win' && e.mode === mode)
                        .map(el => {
                            return <p key={el._id}>{el.userId} 10 points</p>;
                        })}
                </div>
                <div className="difficulty-container">
                    <h2>Normal</h2>

                    {dummyDataMatch
                        .filter(e => e.difficulty === 'normal' && e.result === 'win' && e.mode === mode)
                        .map(el => {
                            return <p key={el._id}>{el.userId} 10 points</p>;
                        })}
                </div>
                <div className="difficulty-container">
                    <h2>Díficil</h2>
                    {dummyDataMatch
                        .filter(e => e.difficulty === 'hard' && e.result === 'win' && e.mode === mode)
                        .map(el => {
                            return <p key={el._id}>{el.userId} 10 points</p>;
                        })}
                </div>
            </div>
        </div>
    );
};

export default Ranking;
