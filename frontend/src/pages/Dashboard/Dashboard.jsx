import React from 'react';

// providers
import { useAuth } from '../../providers';
import AdminDashboard from './Admin';
import ContributorDashboard from './Contributor';
import UserDashboard from './User';
import { CurrentAdminDashboardProvider } from '../../providers';

import './dashboard.scss';

const RoleDashboard = {
    admin: (
        <CurrentAdminDashboardProvider>
            <AdminDashboard></AdminDashboard>
        </CurrentAdminDashboardProvider>
    ),
    user: (
        <CurrentAdminDashboardProvider>
            <AdminDashboard></AdminDashboard>
        </CurrentAdminDashboardProvider>
    ),
    contributor: (
        <CurrentAdminDashboardProvider>
            <AdminDashboard></AdminDashboard>
        </CurrentAdminDashboardProvider>
    ),
};

function Dashboard() {
    const { userProfile } = useAuth();
    return (
        <div className="dashboard">
            <div className="dashboard__container">{RoleDashboard[userProfile.role]}</div>
        </div>
    );
}

export default Dashboard;
