import React, { useEffect, useState } from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer, Tooltip } from 'recharts';
import DatePicker from 'react-date-picker';

//Providers
import { useAdminDashboard } from '../../../../../providers';

function AccessStatistic(props) {
    const { getAllAccess, allAccess } = useAdminDashboard();
    const [fromDate, setFromDate] = useState(new Date(props.from));
    const [toDate, setToDate] = useState(new Date(props.to));

    useEffect(() => {
        if (toDate >= fromDate) {
            getAllAccess(fromDate, toDate);
        }
    }, [toDate, fromDate]);

    return (
        <>
            {allAccess && (
                <div className="statistic">
                    <h2>Número de acessos por dia</h2>
                    <div className="datepicker-container">
                        <DatePicker onChange={setFromDate} value={fromDate} />
                        <DatePicker onChange={setToDate} value={toDate} />
                    </div>
                    <ResponsiveContainer minWidth={600} minHeight={300}>
                        <LineChart data={allAccess.formatToJson()} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                            <Line type="monotone" dataKey="number" stroke="#8884d8" />
                            <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                            <XAxis dataKey="date" />
                            <YAxis />
                            <Tooltip />
                        </LineChart>
                    </ResponsiveContainer>
                </div>
            )}
        </>
    );
}

AccessStatistic.propTypes = {};

export default AccessStatistic;
