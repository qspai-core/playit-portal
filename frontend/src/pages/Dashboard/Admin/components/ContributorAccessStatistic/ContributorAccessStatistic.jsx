import React, { useEffect, useState } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';
import DatePicker from 'react-date-picker';

//Providers
import { useAdminDashboard } from '../../../../../providers';

function ContributorAccessStatistic(props) {
    const { getContributorsAccess, contributorsAccess } = useAdminDashboard();
    const [fromDate, setFromDate] = useState(new Date(props.from));
    const [toDate, setToDate] = useState(new Date(props.to));

    useEffect(() => {
        if (toDate >= fromDate) {
            getContributorsAccess(fromDate, toDate);
        }
    }, [toDate, fromDate]);

    return (
        <>
            {contributorsAccess && (
                <div className="statistic">
                    <h2>Número de partidas por contribuidor</h2>
                    <div className="datepicker-container">
                        <DatePicker onChange={setFromDate} value={fromDate} />
                        <DatePicker onChange={setToDate} value={toDate} />
                    </div>
                    <ResponsiveContainer minWidth={600} minHeight={300}>
                        <BarChart
                            data={contributorsAccess.formatToJson()}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="Name" />
                            <YAxis />
                            <Tooltip />
                            <Bar dataKey="Number" fill="#8884d8" />
                        </BarChart>
                    </ResponsiveContainer>
                </div>
            )}
        </>
    );
}

ContributorAccessStatistic.propTypes = {};

export default ContributorAccessStatistic;
