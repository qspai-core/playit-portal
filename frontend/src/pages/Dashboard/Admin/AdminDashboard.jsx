import React, { useEffect, useState } from 'react';
import AccessStatistic from './components/AccessStatistic';
import ContributorAccessStatistic from './components/ContributorAccessStatistic';

// scss
import './AdminDashboard.scss';

//Providers
import { useAdminDashboard } from '../../../providers';

function AdminDashboard(/* props */) {
    const { getAllAccess, isLoading, allAccess, getContributorsAccess, contributorsAccess } = useAdminDashboard();
    const currDate = new Date();
    const fromDate = new Date().setDate(currDate.getDate() - 15);
    const nextDate = new Date().setDate(currDate.getDate() + 15);

    useEffect(() => {
        getAllAccess(fromDate, nextDate);
        getContributorsAccess(fromDate, nextDate);
    }, []);

    return (
        <>
            {isLoading || !allAccess || !contributorsAccess ? (
                'Loading...'
            ) : (
                <>
                    <AccessStatistic from={fromDate} to={nextDate} />
                    <ContributorAccessStatistic from={fromDate} to={nextDate} />
                </>
            )}
        </>
    );
}

AdminDashboard.propTypes = {};

export default AdminDashboard;
