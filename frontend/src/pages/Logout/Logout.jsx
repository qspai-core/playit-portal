import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

// Providers
import { useAuth } from '../../providers';

function Logout(/*props*/) {
  const { signOut } = useAuth();
  useEffect(() => {
    signOut();
  }, []);

  return <Redirect to="/" />;
}

Logout.propTypes = {
  logout: PropTypes.func,
};

export default Logout;
