/**
 * @author Tomás Santos, João Martins
 * @description
 * Página das estatísticas, desenhada como está no desenho de alto nível
 */

import React, { useState } from 'react';

import './Statistics.scss';

const Statistics = () => {
    //const [mode, setMode] = useState('players');
    const [show, setShow] = useState('Contributors');

    return (
        <div className="statistics__container">
            <h1>Estatísticas</h1>
            <div className="buttons__container">
                <button className="modeButton" type="button" id="singleButton" onClick={() => setShow('Contributors')}>
                    Contribuidores
                </button>
                <button className="modeButton" id="multiButton" onClick={() => setShow('Games')}>
                    Jogos
                </button>
                <button className="modeButton" id="singleButton" onClick={() => setShow('Players')}>
                    Jogadores
                </button>
            </div>
            <div
                className={
                    show === 'Contributors'
                        ? 'statistics__container__contributor show'
                        : 'statistics__container__contributor hide'
                }
            >
                <div className="contributor__container">
                    <h3>Contribuidor</h3>
                    <p>Contributor1</p>
                    <p>Contributor2</p>
                    <p>Contributor3</p>
                </div>
                <div className="time__container">
                    <h3>Registado à:</h3>
                    <p>1 dia</p>
                    <p>2 dia</p>
                    <p>3 dia</p>
                </div>
                <div className="number__games">
                    <h3>Numero total jogos</h3>
                    <p>3</p>
                    <p>2</p>
                    <p>1</p>
                </div>
            </div>
            <div
                className={show === 'Games' ? 'statistics__container__games show' : 'statistics__container__games hide'}
            >
                <div className="games">
                    <h3>Jogos</h3>
                    <p>Jogo 1</p>
                    <p>Jogo 2</p>
                    <p>Jogo 3</p>
                </div>
                <div className="hours__played">
                    <h3>Horas totais de jogo</h3>
                    <p>328</p>
                    <p>200</p>
                    <p>100</p>
                </div>
                <div className="times__played">
                    <h3>Quantidade jogos realizados</h3>
                    <p>100</p>
                    <p>50</p>
                    <p>25</p>
                </div>
            </div>
            <div
                className={
                    show === 'Players' ? 'statistics__container__players show' : 'statistics__container__players hide'
                }
            >
                <div className="players">
                    <h3>Jogadores</h3>
                    <p>Jogador 1</p>
                    <p>Jogador 2</p>
                    <p>Jogador 3</p>
                </div>
                <div className="players__hours__played">
                    <h3>Horas totais de jogo</h3>
                    <p>328</p>
                    <p>200</p>
                    <p>100</p>
                </div>
            </div>
        </div>
    );
};

export default Statistics;
