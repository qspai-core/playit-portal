import React, { useState } from 'react';

import './ForgotPassword.scss';

import FormInput from '../../components/FormInput/FormInput';
import CustomButton from '../../components/CustomButton';

import { request } from '../../utils/requests';

const ForgotPassword = () => {
    const [userEmail, setUserEmail] = useState({
        email: '',
    });
    const { email } = userEmail;

    const handleSubmit = event => {
        event.preventDefault();
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
        };

        const url = `${process.env.REACT_APP_BACKEND_URL}/user/recover/${email}`;
        request(url, requestOptions, 'Email Sent', function(res) {});
    };

    const handleChange = event => {
        const { value, name } = event.target;

        setUserEmail({ ...userEmail, [name]: value });
    };

    return (
        <div className="forgot-page">
            <h2>Forgot your password?</h2>
            <span>If your account exists we will send you an email to recover the password</span>
            <form onSubmit={handleSubmit}>
                <FormInput
                    name="email"
                    type="email"
                    value={email}
                    handleChange={handleChange}
                    label="Insert Email"
                    required
                />
                <CustomButton type="submit"> Send Email </CustomButton>
            </form>
        </div>
    );
};

export default ForgotPassword;
