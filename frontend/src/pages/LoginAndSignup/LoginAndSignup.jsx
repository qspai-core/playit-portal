import React, { useState } from 'react';
import { useAuth } from '../../providers';
import { Redirect } from 'react-router-dom';

import Login from '../../components/Login/Login';
import SignUp from '../../components/SignUp/SignUp';

import './LoginAndSignup.scss';

/* const { innerWidth: width } = window; */

function LoginAndSignup(props) {
    const [isLogin, setIsLogin] = useState(true);
    const { isAuthenticated } = useAuth();

    const handleEvent = event => {
        setIsLogin(!isLogin);
    };

    if (isAuthenticated) {
        const { from = '/dashboard' } = props.location.state || {};
        return <Redirect to={from} />;
    }

    return (
        <div className="login-and-signup">
            <div className={isLogin ? 'showLogin' : 'hideLogin'}>
                <Login />
                <div className="mobile_register_wrapper">
                    <p>Ainda não tem conta ?</p>
                    <button className="mobile_switch_button" onClick={handleEvent}>
                        Registe-se
                    </button>
                </div>
            </div>
            <div className={!isLogin ? 'ShowLogin' : 'hideLogin'}>
                <SignUp />
                <div className="mobile_register_wrapper">
                    <p>Já tem conta?</p>
                    <button className="mobile_switch_button" onClick={handleEvent}>
                        Login
                    </button>
                </div>
            </div>
        </div>
    );
}

export default LoginAndSignup;
