import React, { useEffect, useState } from 'react';

import './ConfirmEmail.scss';

const ConfirmEmail = ({ location }) => {
    const id = location.search.split('?')[1];
    const [confirmed, setConfirmed] = useState({
        confirmed: false,
        text: '',
    });

    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_BACKEND_URL}/user/validate/${id}`, requestOptions)
            .then(response => response.json())
            .then(function(e) {
                if (e.success) {
                    setConfirmed({
                        confirmed: true,
                        text: 'Your email has been confirmed!',
                    });
                } else {
                    setConfirmed({
                        confirmed: false,
                        text: e.error.message,
                    });
                }
            });
    }, []);

    return (
        <div className="confirm-page">
            <h2>Confirm Email</h2>
            <span>{confirmed.text}</span>
        </div>
    );
};

export default ConfirmEmail;
