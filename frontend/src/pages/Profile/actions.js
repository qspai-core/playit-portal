import { header, request } from "../../utils/requests";

export async function changePassword(formData, id, cb){
    const requestOptions = {
        method: 'PUT',
        headers: header(),
        body: JSON.stringify(formData),
    };
    
    await request(`${process.env.REACT_APP_BACKEND_URL}/user/${id}`, 
        requestOptions,
        "Perfil Editado com sucesso.", cb);
}