import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Form, Input } from '@rocketseat/unform';
import AvatarInput from './AvatarInput';

//Actions
import { changePassword } from './actions';

// Providers
import { useAuth } from '../../providers';

// scss
import './Profile.scss';

function Profile(props) {
    const [avatar, setAvatar] = useState(null);
    const { userProfile, signOut, getUserProfile } = useAuth();
    //const { hello, changeHelloWord } = useHello();

    async function HandleSubmit(data) {
        data.avatar = avatar;
        changePassword(data, userProfile._id, getUserProfile);
    }

    function HandleSignOut() {
        signOut();
        props.history.push('/');
    }

    useEffect(() => {}, []);

    return (
        <div className="profile">
            <p>315 jogos - 200 horas de jogo - ...</p>
            <Form initialData={userProfile} onSubmit={HandleSubmit}>
                <AvatarInput name="avatar_id" url={userProfile.avatar} onChange={file => setAvatar(file)} />

                <Input name="name" placeholder="fullname" />
                <Input name="mail" type="email" placeholder="your email adress" />
                <hr />

                <Input type="password" name="old_password" placeholder="Current Password" />
                <Input type="password" name="new_password" id="newPassword" placeholder="New Password" />
                <Input type="password" name="confirm_password" id="ConfirmPassword" placeholder="Confirm Password" />

                <div className="profile__buttons__container">
                    <button className="update__btn" type="submit">
                        Confirm
                    </button>
                    <vr />
                    <button className="logout__btn" type="button" onClick={HandleSignOut}>
                        Logout
                    </button>
                </div>
            </Form>
        </div>
    );
}

Profile.propTypes = {};

export default withRouter(Profile);
