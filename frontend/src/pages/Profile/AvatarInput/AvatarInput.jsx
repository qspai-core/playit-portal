import React, { useState } from 'react';
import Profile_img from '../../../assets/images/Profile_img.png';

// scss
import './AvatarInput.scss';

function AvatarInput( props) {
    const [preview, setPreview] = useState(props.url);

    function uploadImage(onImageUploaded) {
        let input = document.getElementById("avatar");
    
        let reader = new FileReader();
        reader.onloadend = function() {
          onImageUploaded(reader.result);
          setPreview(reader.result);
          // Reset input to allow upload when image is removed
          input.value = "";
        };
    
        if (input.files[0]) {
          reader.readAsDataURL(input.files[0]);
        }
      }

    return (
        <div className="avatar__input">
            <label htmlFor="avatar">
                <img src={preview || props.url || Profile_img} alt="" />
                <input type="file" id="avatar" accept="image/*" onChange={() => 
                    uploadImage(props.onChange)
                } />
            </label>
        </div>
    );
}

AvatarInput.propTypes = {};

export default AvatarInput;
