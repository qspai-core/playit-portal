import React from 'react';

// scss
import './SomePrivatePage.scss';
// import { PrivatePage } from './styles';

function SomePrivatePage(/* props */) {
  return (
    <div className="some-private-page">
      <h1>I am some private page!</h1>
    </div>
  );
}

export default SomePrivatePage;
