require('dotenv').config()

module.exports = {
    url: process.env.URL,
    port: process.env.PORT,
    log4js: {
        file: "./config/log4js.config.json",
        appender: "dateFile",
        level: "debug"
    },
    bcrypt: {
        salt: "$2a$10$WjE5Prw4RiysLhUbbpfBXO"
    },
    mongo: {
        connectionString: `mongodb+srv://${process.env.ATLAS_USER}:${process.env.ATLAS_KEY}@core-prd-01.plebw.mongodb.net/qs-pai?retryWrites=true&w=majority`,
        dbName: process.env.DATABASE_NAME
    },
    email: {
        service: 'gmail',
        from: 'qspai.playit@gmail.com',
        auth: {
            user: 'qspai.playit@gmail.com',
            pass: '@B4yCVfqX*ewA$@w'
        }
    },
    role: {
        contributor: 'contributor',
        player: 'player',
        admin: 'admin'
    }
};
