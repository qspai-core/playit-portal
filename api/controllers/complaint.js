/**
 * @module {{}} complaint
 * @parent api.controllers
 * @author Paulo Fournier
 * @description
 * Objeto que faz a gestão das denuncias
 */


const dotenv = require("dotenv");
dotenv.config();

const config = require("../../config/settings"),
    DBRef = require("mongodb").DBRef,
    ObjectID = require("mongodb").ObjectID;

let logger = null;

module.exports = {

    /**
     * Função para inicializar o módulo
     * @param {log4js} log4js O logger
     * @param {MongoClient} O cliente da BD
     * @return {void}
     */
    init: function (log4js, dbClient) {

        this.dbClient = dbClient;

        logger = log4js.getLogger("user");
        logger.level = config.log4js.level;

    },
    /**
     * Função para obter todos as denuncias
     * @param {Number} skip Número de registos a ignorar
     * @param {Number} limit Limite máximo de registos no cursor
     * @return {Promise} Lista com todos as denuncias
     */
    getAllComplaint: async function (skip, limit) {

        let complaint = [],
            cursor = await this.dbClient
                .db(config.mongo.dbName)
                .collection("complaint")
                .find(
                    {}
                )
                .skip(skip)
                .limit(limit);

        if ((await cursor.count()) === 0) {
            return complaint;
        }

        await cursor.forEach((r) => { complaint.push(r); });

        return complaint;

    },
    /**
     * Função para obter uma denuncia pelo seu id
     * @param {ObjectId} _id O id unico da denuncia (ObjectId)
     * @return {Promise} O objeto MongoDB da Denuncia
     */
    getComplaintById: async function (_id) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("complaint")
            .findOne(
                {
                    _id: _id
                }
            );

    },
    /**
    * Função para obter uma denuncia pelo id do denunciador
    * @param {ObjectId} _id O id do jogador DBRef(user, id)
    * @return {Promise} O objeto MongoDB da Denuncia
    */
    getComplaintByUserId: async function (_id) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("complaint")
            .findOne(
                {
                    //mudar para createdBy.$id
                    "createdBy.$id": _id
                }
            );

    },
    /**
     * Função utilizada para arquivar uma denuncia
     * @param {string} id A string do ObjectId
     * @param {string} userId O id do user que arquivou a denuncia
     * @param {boolean} archived O estado da denuncia
     * @param {function} cb A função de callback
     */
    setArchived: async function (id, userId, archived, cb) {

        let _id = null;

        try {
            _id = new ObjectID(id);
        } catch (e) {
            return cb({
                code: 7,
                message: `Denuncia _id ${id} não é um ObjectId válido`
            }, null);
        }

        //definir o estado
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("complaint")
            .updateOne(
                {
                    "_id": _id
                },
                {
                    "$set": {
                        "isArchived": archived,
                        "archivedBy": new DBRef("user", new ObjectID(userId))
                    }
                }
            );

        logger.info('Complaint archived with _id:%s was set:%d', id, archived);

        cb(null);

    },
    /**
     * Função utilizada para adicionar uma nova resposta a uma denuncia
     * @param {string} id A string do ObjectId
     * @param {string} userId O id do user que arquivou a denuncia
     * @param {string} response Uma resposta à denuncia
     * @param {function} cb A função de callback
     */
    addResponse: async function (id, userId, response, cb) {

        let _id = null;

        try {
            _id = new ObjectID(id);
        } catch (e) {
            return cb({
                code: 7,
                message: `Denuncia _id ${id} não é um ObjectId válido`
            }, null);
        }

        //definir o estado
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("complaint")
            .updateOne(
                {
                    "_id": _id
                },
                {
                    "$push": {
                        "responses": {
                            "text": response,
                            "userId": new DBRef("user", new ObjectID(userId)),
                            "date": new Date()
                        }
                    }
                }
            );

        logger.info('Add response to complaint with _id:%s, response:%s', id, response);

        cb(null);

    },
    /**
     * Função para adicionar uma nova denúncia
     * @param {string} userId O id unico do jogo
     * @param {string} gameId O id unic do Jogo
     * @param {string} title O titulo da denuncia
     * @param {string} description A descrição da denuncia
     * @return {Promise<void>} Retorna uma promessa com o resultado
     */
    addComplaint: async function (userId, gameId, title, description) {

        return await new Promise(async (resolve, reject) => {

            let _idUser = null,
                _idGame = null;

            try {
                _idUser = new ObjectID(userId);
            } catch (e) {
                return reject({
                    code: 7,
                    message: `Denuncia userId ${userId} não é um ObjectId válido`
                });
            }

            try {
                _idGame = new ObjectID(gameId);
            } catch (e) {
                return reject({
                    code: 7,
                    message: `Jogo gameId ${gameId} não é um ObjectId válido`
                });
            }

            //inserir utilizador
            const rst = await this.dbClient
                .db(config.mongo.dbName)
                .collection("complaint")
                .insertOne(
                    {
                        "title": title,
                        "description": description,
                        "isArchived": false,
                        "archivedBy": null,
                        "createdBy": new DBRef("user", _idUser),
                        "gameId": new DBRef("user", _idGame),
                        "responses": []
                    }
                );

            if(!rst)
                return reject({
                    code: 8,
                    message: `Denuncia não adicionada`
                });

            logger.info('Complaint inserted with _id:%s, user id:%s', rst.insertedId, userId);

            return resolve({ _id: rst.insertedId });

        });

    }

};