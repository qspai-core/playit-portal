/**
 * @module {{}} auth
 * @author Paulo Fournier
 * @description
 * Objeto que faz a validação do utilizador
 */

const config = require("../../config/settings"),
    bcrypt = require("bcryptjs");

let logger = null;

module.exports = {

    /**
     * Função para inicializar o módulo
     * @param {log4js} log4js O logger
     * @param {MongoClient} O cliente da BD
     * @return {void}
     */
    init: function (log4js, dbClient) {

        this.dbClient = dbClient;

        logger = log4js.getLogger("user");
        logger.level = config.log4js.level;
    },
    /**
     * Valida o acesso à API recebendo o username e password do Basic Auth
     * @param {string} email O nome do utilizador
     * @param {string} password O password do utilizador
     * @param {function} cb A função de callback
     * @return {void}
     */
    validate: async function (email, password, cb) {
        //converter password para o hash
        const hash = bcrypt.hashSync(password, config.bcrypt.salt);

        //procurar utilizador com as credenciais
        const user = await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .findOne(
                {
                    email: email,
                    password: hash
                },
                {
                    projection: {
                        password: 0
                    }
                }
            );

        if (!user)
            return cb({
                code: 4,
                message: "Autenticação inválida"
            }, null);

        const role = await this.dbClient
            .db(config.mongo.dbName)
            .collection("role")
            .findOne({ _id: user.role.oid });

        //add session history
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("user_hist")
            .insertOne({
                "userId": new DBRef("user", user._id),
                "date": new Date()
            });

        logger.info('User auth successfully for email:%s', email);

        //apenas retorna true se se o user for encontrado, estiver ativo e se tiver a conta de email validada
        return cb(null, {
            "_id": user._id.toString(),
            "name": user.name,
            "birth_date": !user.birthDate ? null : user.birthDate.toISOString(),
            "mail": user.email,
            "avatar": user.avatar,
            "role": role.name
        });
    },
    /**
     * Valida se existe um utilizador com o id e a password dadas
     * @param {string} userId O id do utilizador
     * @param {string} password O password do utilizador
     * @param {function} cb A função de callback
     * @return {void}
     */
    validatePassword: async function (userId, password, cb) {
        //converter password para o hash
        const hash = bcrypt.hashSync(password, config.bcrypt.salt);

        //procurar utilizador com as credenciais
        const user = await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .findOne(
                {
                    _id: userId,
                    password: hash
                },
                {
                    projection: {
                        password: 0
                    }
                }
            );

        if (!user)
            return cb({
                code: 4,
                message: "Utilizador com esta password e id inexistente."
            }, null);

        logger.info('User password validation successfully checked for id:%s', userId);

        //apenas retorna true se se o user for encontrado
        return cb(null, {
            "_id": user._id.toString(),
            "name": user.name,
            "birth_date": !user.birthDate ? null : user.birthDate.toISOString(),
            "mail": user.email,
            "avatar": user.avatar
        });
    }

};