/**
 * @module {{}} user
 * @parent api.controllers
 * @author Paulo Fournier
 * @description
 * Objeto que faz a gestão do utilizador
 */


const dotenv = require("dotenv");
dotenv.config();

const config = require("../../config/settings"),
    bcrypt = require("bcryptjs"),
    ObjectID = require("mongodb").ObjectID,
    DBRef = require("mongodb").DBRef,
    nodemailer = require('nodemailer'),
    fileManager = require("../../FileManager/fileManager");

let logger = null;

module.exports = {

    /**
     * Função para inicializar o módulo
     * @param {log4js} log4js O logger
     * @param {MongoClient} O cliente da BD
     * @return {void}
     */
    init: function (log4js, dbClient) {

        this.dbClient = dbClient;

        logger = log4js.getLogger("user");
        logger.level = config.log4js.level;

    },
    /**
     * Função para verificar se o email já existe na BD
     * @param {string} email
     * @return {Promise} O objeto MongoDB do User
     */
    checkIfMailExists: async function (email) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .findOne(
                {
                    email: email
                },
                {
                    projection: {
                        _id: 1
                    }
                }
            );

    },
    /**
     * Função para verificar se o email já existe na BD
     * @param {ObjectId} _id The role ObjectId
     * @return {Promise} O objeto MongoDB do Role
     */
    checkIfRoleExists: async function (_id) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("role")
            .findOne(
                {
                    _id: _id
                },
                {
                    projection: {
                        _id: 1,
                        name: 1
                    }
                }
            );

    },
    /**
     * Função para obter um utilizador pelo seu id
     * @param {ObjectId} _id The user ObjectId
     * @return {Promise} O objeto MongoDB do User
     */
    getUserById: async function (_id) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .findOne(
                {
                    _id: _id
                },
                {
                    projection: {
                        password: 0
                    }
                }
            );

    },
    /**
         * Função para obter um utilizador pelo seu id
         * @param {ObjectId} _id The user ObjectId
         * @return {Promise} O objeto MongoDB do User
         */
    getUserByIdWithPassword: async function (_id) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .findOne(
                {
                    _id: _id
                },
            );

    },
    /**
     * Função para obter todos os utilizador
     * @param {Number} skip Número de registos a ignorar
     * @param {Number} limit Limite máximo de registos no cursor
     * @return {Promise} Lista com todos os utilizadores
     */
    getAllUsers: async function (skip, limit) {

        let users = [],
            cursor = await this.dbClient
                .db(config.mongo.dbName)
                .collection("user")
                .find(
                    {},
                    {
                        projection: {
                            password: 0
                        }
                    }
                )
                .skip(skip)
                .limit(limit);

        if ((await cursor.count()) === 0) {
            return users;
        }

        await cursor.forEach((r) => { users.push(r); });

        return users;

    },
    /**
     * Função para obter todos os contribuidores
     * @param {Number} skip Número de registos a ignorar
     * @param {Number} limit Limite máximo de registos no cursor
     * @return {Promise} Lista com todos os contribuidores
     */
    getAllContributors: async function (skip, limit) {
        const role = await this.dbClient
            .db(config.mongo.dbName)
            .collection("role")
            .findOne({
                name: config.role.contributor
            });

        let contributors = [],
            cursor = await this.dbClient
                .db(config.mongo.dbName)
                .collection("user")
                .find(
                    {
                        "role.$id": role._id
                    },
                    {
                        projection: {
                            password: 0
                        }
                    }
                )
                .skip(skip)
                .limit(limit);

        if ((await cursor.count()) === 0) {
            return contributors;
        }

        await cursor.forEach((r) => { contributors.push(r); });


        return contributors;
    },
    /**
   * Função para obter todos os jogadores
   * @param {Number} skip Número de registos a ignorar
   * @param {Number} limit Limite máximo de registos no cursor
   * @return {Promise} Lista com todos os jogadores
   */
    getAllPlayers: async function (skip, limit) {
        const role = await this.dbClient
            .db(config.mongo.dbName)
            .collection("role")
            .findOne({
                name: config.role.player
            });
        let players = [],
            cursor = await this.dbClient
                .db(config.mongo.dbName)
                .collection("user")
                .find(
                    {
                        "role.$id": role._id
                    },
                    {
                        projection: {
                            password: 0
                        }
                    }
                )
                .skip(skip)
                .limit(limit);

        if ((await cursor.count()) === 0) {
            return players;
        }

        await cursor.forEach((r) => { players.push(r); });


        return players;
    },
    /**
     * Função para obter um utilizador pelo seu email
     * @param {string} email The email address
     * @return {Promise} O objeto MongoDB do User
     */
    getUserByEmail: async function (email) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .findOne(
                {
                    email: email
                }
            );

    },
    /**
     * Função para adicionar um novo utilizador
     * @param {string} email Conta de email (tem de ser unico no sistema)
     * @param {string} password Password do utilizador em plain-text
     * @param {string} name Nome do utilizador
     * @param {date} birth_date Data de nascimento do utilizador
     * @param {string} avatar Imagem associada ao utilizado
     * @param {function} cb Função callback
     * @return {void}
     */
    addUser: async function (email, password, name, birth_date, avatar, roleId, cb) {

        //veririfica se o email é unico no sistema
        if (await this.checkIfMailExists(email)) {

            return cb({
                code: 6,
                message: `Conta de email ${email} já existente`
            }, null);

        }

        let roleObjectId = null;

        try {
            roleObjectId = await this.checkIfRoleExists(new ObjectID(roleId));
        } catch (e) {
            return cb({
                code: 7,
                message: `Role _id ${roleId} não é um ObjectId válido`
            }, null);
        }

        //verifica se o role existe
        if (!roleObjectId) {

            return cb({
                code: 6,
                message: `Role _id ${roleId} não encontrada`
            }, null);

        }

        //inserir utilizador
        const rst = await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .insertOne(
                {
                    "avatar": avatar,
                    "name": name,
                    "password": bcrypt.hashSync(password, config.bcrypt.salt),
                    "email": email,
                    "emailConfirmed": false,
                    "isActive": false,
                    "birthDate": birth_date,
                    "role": new DBRef("role", roleObjectId._id)
                }
            );

        if (rst.insertedId) {
            let html = `Carregue no link seguinte para ativar a vossa <a href="${config.url}activate?${rst.insertedId}">conta</a>.`;
            this.sendMail(email, 'Mail de ativação na conta PLAYIT', html);
        }

        logger.info('User inserted with _id:%s, username:%s and role %s', rst.insertedId, email, roleObjectId.name);

        return cb(null, { _id: rst.insertedId });

    },
    /**
     * General function to send an email
     * @param {string} to O email do destinatário
     * @param {string} html O corpo do email (HTML)
     */
    sendMail: function (to, subject, html) {

        //autenticar no smtp
        let transporter = nodemailer.createTransport({
            service: config.email.service,
            auth: {
                user: config.email.auth.user,
                pass: config.email.auth.pass
            }
        });

        //defenir opções
        let mailOptions = {
            from: config.email.from,
            to: to,
            subject: subject,
            html: html
        };

        //enviar email
        transporter.sendMail(mailOptions, function (error, info) {

            if (error) {
                logger.error('Email not sent to %s with error:%s', to, error.message);
            } else {
                logger.info('Mail sent to %s with html:[%s]. response:[%s]', to, html, info.response);
            }

        });

    },
    /**
     * Função utilizada para ativar uma conta de um utilizador
     * @param {string} id A string do ObjectId
     * @param {function} cb A função de callback
     */
    validateAccount: async function (id, cb) {

        let user = null;

        try {
            user = await this.getUserById(new ObjectID(id));
        } catch (e) {
            return cb({
                code: 7,
                message: `User _id ${id} não é um ObjectId válido`
            }, null);
        }

        if (!user)
            return cb({
                code: 8,
                message: `User not found for _id ${id}`
            }, null);

        //verifica se o utilizar já se encontra ativado
        if (user.emailConfirmed)
            return cb({
                code: 9,
                message: `User with _id ${id} already activated`
            }, null);

        //ativar utilizador
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .updateOne(
                {
                    "_id": new ObjectID(id)
                },
                {
                    "$set": {
                        "emailConfirmed": true,
                        "isActive": true
                    }
                }
            );

        logger.info('User with _id:%s and username:%s was activated', id, user.email);

        cb(null);

    },
    /**
     * Função para alterar a password de um utilizador
     * @param {string} id A string do ObjectId
     * @param {string} password A nova password do utilizador
     * @param {function} cb A função de callback
     * @return {void}
     */
    setPassword: async function (id, password, cb) {
        let user = null;

        try {
            user = await this.getUserById(new ObjectID(id));
        } catch (e) {
            return cb({
                code: 7,
                message: `User _id ${id} não é um ObjectId válido`
            }, null);
        }

        if (!user)
            return cb({
                code: 8,
                message: `User not found for _id ${id}`
            }, null);

        //alterar informações do utilizador
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .updateOne(
                {
                    "_id": new ObjectID(id)
                },
                {
                    "$set": {
                        "password": bcrypt.hashSync(password, config.bcrypt.salt)
                    }
                }
            );

        logger.info('Password and avatar updated for user %s with _id:%s', user.email, id);

        return cb(null);
    },
    /**
     * Função para alterar a password de um utilizador
     * @param {string} id A string do ObjectId
     * @param {string} password A nova password do utilizador
     * @param {string} avatar O novo avatar do utilizador
     * @param {function} cb A função de callback
     * @return {void}
     */
    updatePasswordAndProfilePicture: async function (id, password, avatar, cb) {
        let user = null;

        try {
            user = await this.getUserByIdWithPassword(new ObjectID(id));
        } catch (e) {
            return cb({
                code: 7,
                message: `User _id ${id} não é um ObjectId válido`
            }, null);
        }

        if (!user)
            return cb({
                code: 8,
                message: `User not found for _id ${id}`
            }, null);

        let avatarUrl = !avatar ? user.avatar : await fileManager.uploadBase64(avatar);
        if (!avatarUrl && avatar)
            return cb({
                code: 10,
                message: `Something went wrong uploading the avatar`
            }, null);

        //alterar informações do utilizador
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .updateOne(
                {
                    "_id": new ObjectID(id)
                },
                {
                    "$set": {
                        "password": password ? bcrypt.hashSync(password, config.bcrypt.salt) : user.password,
                        "avatar": avatarUrl
                    }
                }
            );

        logger.info('Password and avatar updated for user %s with _id:%s', user.email, id);

        return cb(null);
    },
    /**
     * Função que envia um email de recuperação de conta para o utilizador
     * @param {string} email O email do utilizador
     * @param {function} cb A função de callback
     * @return {Promise}
     */
    sendRecoverMail: async function (email, cb) {

        let user = await this.getUserByEmail(email);

        if (!user)
            return cb({
                code: 8,
                message: `User not found for email ${email}`
            }, null);


        let html = `Carregue no link seguinte para defenir uma nova <a href="${config.url}recover?${user._id.toString()}">password</a>.`;
        this.sendMail(user.email, 'Mail de recuperação da conta PLAYIT', html);

        logger.info('Account recover for user %s with _id:%s', user.email, user._id.toString());

        cb(null);

    },
    /**
     * Função para editar um utilizador
     * @param {string} id Identificação única do utilizador (ObjectId)
     * @param {string} name Nome do utilizador
     * @param {Date} birth_date Data de nascimento
     * @param {Blob} avatar Imagem associada ao utilizado
     * @param {string} roleId Id do papel do utilizador (ObjectId)
     * @param {function} cb Função de callback do utilizador
     * @return {void}
     */
    setUser: async function (id, name, birth_date, avatar, roleId, cb) {

        let roleObjectId = null,
            _id = null;

        try {
            _id = new ObjectID(id);
        } catch (e) {
            return cb({
                code: 7,
                message: `User _id ${id} não é um ObjectId válido`
            }, null);
        }

        try {
            roleObjectId = await this.checkIfRoleExists(new ObjectID(roleId));
        } catch (e) {
            return cb({
                code: 7,
                message: `Role _id ${roleId} não é um ObjectId válido`
            }, null);
        }

        //verifica se o role existe
        if (!roleObjectId) {
            return cb({
                code: 6,
                message: `Role _id ${roleId} não encontrada`
            }, null);
        }

        //atualizar utilizador
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .updateOne(
                {
                    _id: _id
                },
                {
                    '$set': {
                        "avatar": avatar,
                        "name": name,
                        "birthDate": birth_date,
                        "role": new DBRef("role", roleObjectId._id)
                    }
                }
            );

        logger.info('User updated with success _id:%s', id);

        return cb(null, null);

    },
    /**
     * Função utilizada para ativar uma conta de um utilizador
     * @param {string} id A string do ObjectId
     * @param {boolean} active O estado do utilizador
     * @param {function} cb A função de callback
     */
    setActive: async function (id, active, cb) {

        let _id = null;

        try {
            _id = new ObjectID(id);
        } catch (e) {
            return cb({
                code: 7,
                message: `User _id ${id} não é um ObjectId válido`
            }, null);
        }

        //definir o estado
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("user")
            .updateOne(
                {
                    "_id": _id
                },
                {
                    "$set": {
                        "isActive": active
                    }
                }
            );

        logger.info('User state with _id:%s was set:%d', id, active);

        cb(null);

    }

};