/**
 * @module {{}} game
 * @parent api.controllers
 * @author Paulo Fournier
 * @description
 * Objeto que faz a gestão do jogo
 */


const dotenv = require("dotenv");
dotenv.config();

const config = require("../../config/settings"),
    userController = require("./user")
    ObjectID = require("mongodb").ObjectID,
    DBRef = require("mongodb").DBRef;

let logger = null;

module.exports = {

    /**
     * Função para inicializar o módulo
     * @param {log4js} log4js O logger
     * @param {MongoClient} O cliente da BD
     * @return {void}
     */
    init: function (log4js, dbClient) {

        this.dbClient = dbClient;

        logger = log4js.getLogger("user");
        logger.level = config.log4js.level;

    },
    /**
     * Função para obter todos os jogos
     * @param {Number} skip Número de registos a ignorar
     * @param {Number} limit Limite máximo de registos no cursor
     * @return {Promise} Lista com todos os jogos
     */
    getAllGames: async function(skip, limit) {

        let games = [],
            cursor = await this.dbClient
                .db(config.mongo.dbName)
                .collection("game")
                .find(
                    {},
                    {
                        projection: {
                            complaints: 0,
                            matches: 0,
                            publications: 0
                        }
                    }
                )
                .skip(skip)
                .limit(limit);

        if ((await cursor.count()) === 0) {
            return games;
        }

        await cursor.forEach((r) => {games.push(r);});

        return games;

    },
    /**
     * Função para obter um utilizador pelo seu id
     * @param {ObjectId} _id O id unico do jogo (ObjectId)
     * @return {Promise} O objeto MongoDB do Jogo
     */
    getGameById: async function(_id) {

        return await this.dbClient
            .db(config.mongo.dbName)
            .collection("game")
            .findOne(
                {
                    _id: _id
                }
            );

    },
    /**
     * Função utilizada para ativar um jogo
     * @param {string} id A string do ObjectId
     * @param {boolean} active O estado do jogo
     * @param {function} cb A função de callback
     */
    setActive: async function (id, active, cb) {

        let _id = null;

        try {
            _id = new ObjectID(id);
        } catch (e) {
            return cb({
                code: 7,
                message: `Jogo _id ${id} não é um ObjectId válido`
            }, null);
        }

        //definir o estado
        await this.dbClient
            .db(config.mongo.dbName)
            .collection("game")
            .updateOne(
                {
                    "_id": _id
                },
                {
                    "$set": {
                        "isActive": active
                    }
                }
            );

        logger.info('Game state with _id:%s was set:%d', id, active);

        cb(null);
    },

    /**
     * Função para adicionar um novo jogo
     * @param {string} image Imagem do jogo
     * @param {string} name Nome do jogo
     * @param {string} url Url onde do jogo está a funcionar
     * @param {string} ownerId Id do utilizador
     * @param {function} cb Função callback
     * @return {void}
     */
    addGame: async function (image, name, url, ownerId, cb) {
        let user = null;
        //veririfica se o user id existe
        try {
            user = await userController.getUserById(new ObjectID(ownerId));
        } catch (e) {
            return cb({
                code: 7,
                message: `User _id ${id} não é um ObjectId válido`
            }, null);
        }
     
        //verifica se o role existe
        if (!user) {
            return cb({
                code: 8,
                message: `User not found for _id ${id}`
            }, null);
        }

        let imageUrl = image ? await fileManager.uploadBase64(avatar) : null;
        if (!imageUrl && image)
            return cb({
                code: 11,
                message: `Something went wrong uploading the game image`
            }, null);

        //inserir utilizador
        const rst = await this.dbClient
            .db(config.mongo.dbName)
            .collection("game")
            .insertOne(
                {
                    "image": image,
                    "url": url,
                    "name": name,
                    "ownerId": new DBRef("user", user._id)
                }
            );

        logger.info('Game inserted with _id:%s, name:%s and url %s', rst.insertedId, name, url);

        return cb(null, { _id: rst.insertedId });
    },
    /**
     * Retorna todos os jogos ativos de um contribuidor
     * @param {ObjectId} contributorId O id unico do contribuidor
     * @return {Promise<*>} Lista com todos os jogos
     */
    getContributorGames: async function(contributorId) {

        let games = [],
            cursor = await this.dbClient
            .db(config.mongo.dbName)
            .collection("game")
            .find(
                {
                    "ownerId.$id": contributorId,
                    "isActive": true
                },
                {
                    projection: {
                        complaints: 0,
                        matches: 0,
                        publications: 0
                    }
                }
            );

        await cursor.forEach((r) => {

            games.push(r);

        });

        return games;

    }

};