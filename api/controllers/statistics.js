/**
 * @module {{}} statistics
 * @parent api.controllers
 * @author Paulo Fournier
 * @description
 * Objeto que faz a gestão das estatisticas
 */


const dotenv = require("dotenv");
dotenv.config();

const config = require("../../config/settings"),
    ObjectID = require("mongodb").ObjectID;

let logger = null;

module.exports = {

    /**
     * Função para inicializar o módulo
     * @param {log4js} log4js O logger
     * @param {MongoClient} O cliente da BD
     * @return {void}
     */
    init: function (log4js, dbClient, gameController, userController) {

        this.gameController = gameController;
        this.userController = userController;
        this.dbClient = dbClient;

        logger = log4js.getLogger("statistics");
        logger.level = config.log4js.level;

    },
    /**
     * Interface que retorna o total de acessos à plataforma por jogo e no total
     * @param {ObjectID} gameId O id Unico do jogo (null para retornar todos)
     * @param {Date} from Data minima da partida
     * @param {Date} to Data máxima para a partida
     * @return {Promise<unknown>}
     */
    getGameTotals: async function(gameId, from, to) {

        return await new Promise(async (resolve, reject) => {

            let find = {
                    "_id": {
                        "$gte": ObjectID.createFromTime(from.getTime() / 1000),
                        "$lte": ObjectID.createFromTime(to.getTime() / 1000)
                    }
                };

            //se tiver o gameId define, filtra por esse jogo
            //caso contrario, devolve as estatisticas para todos os jogos
            if(gameId)
                find["gameId.$id"] = gameId;

            let match = [],
                gameList = [],
                sum = 0,
                durationSum = 0;

            //obter a lista de todas as partidas, agrupadas por jogo
            let cursor = await this.dbClient
                    .db(config.mongo.dbName)
                    .collection("match")
                    .aggregate([
                        {
                            $match: find
                        },
                        {
                            $group: {
                                _id: "$gameId",
                                count: { $sum: 1 },
                                duration: { $sum: "$duration" }
                            }
                        }
                    ]);

            await cursor.forEach((r) => {

                gameList.push(r._id.oid);

                sum += r.count;
                durationSum += r.duration;

                match.push({
                    "_id": r._id.oid,
                    "name": null,
                    "count": r.count,
                    "duration": r.duration
                });

            });

            //obter informação extra de cada jogo
            cursor = await this.dbClient
                .db(config.mongo.dbName)
                .collection("game")
                .find(
                    {_id:{"$in": gameList}},
                    {
                        projection: {_id:1, name:1}
                    }
                )

            await cursor.forEach((r) => {

                match.forEach((v,i) => {
                    if(v._id.toString() === r._id.toString()) {
                        v.name = r.name;
                    }
                })

            });

            return resolve({
                "total": sum,
                "totalDuration": durationSum,
                "games": match
            });

        });

    },
    /**
     * Interface que retorna o total de acessos à plataforma por jogador
     * @param {Date} from Data minima da partida
     * @param {Date} to Data máxima para a partida
     * @return {Promise<unknown>}
     */
    getUserTotals: async function(from, to) {

        return await new Promise(async (resolve, reject) => {

            if(to > new Date())
                to = new Date();

            let days = {},
                count = 0,
                fromTime = from.getTime(),
                toTime = to.getTime();

            while (fromTime <= toTime) {

                let day = new Date(fromTime).toISOString().substr(0,10);
                days[day] = 0;

                fromTime += (24 * 3600 * 1000);

            }

            //obter a lista de todas as partidas, agrupadas por jogo
            let cursor = await this.dbClient
                .db(config.mongo.dbName)
                .collection("user_hist")
                .find(
                    {
                        "date": {
                            "$gte": from,
                            "$lte": to
                        }
                    },
                    {
                        "projection": {
                            _id: 0,
                            date: 1
                        }
                    }
                );

            await cursor.forEach((r) => {

                let day = r.date.toISOString().substr(0,10);

                ++count;
                ++days[day];

            });

            let daysList = [];

            for(let day in days) {

                daysList.push({
                    "date": day,
                    "count": days[day]
                });

            }

            return resolve({
                "total": count,
                "grouped": daysList
            });

        });

    },
    /**
     * Interface que retorna o total de acessos à plataforma por jogador
     * @param {ObjectId} contributorId unique Id
     * @param {Date} from Data minima da partida (null para tudo)
     * @param {Date} to Data máxima para a partida (null para tudo)
     * @return {Promise<unknown>}
     */
    getUserTotalsPerContributor: async function(contributorId, from, to) {

        return await new Promise(async (resolve, reject) => {

            let gameIdList = [],
                gameRefList = {},
                match = [],
                gameList = [],
                sum = 0,
                durationSum = 0,
                games = await this.gameController.getContributorGames(contributorId);

            if (games.length) {

                for(let i=0; i<games.length; i++) {
                    gameIdList.push(games[i]._id);
                    gameRefList[games[i]._id.toString()] = games[i];
                }

                let find = {
                    "gameId.$id": {
                        "$in": gameIdList
                    }
                };

                //filtra os resultados por data, se necessário
                if(from && to) {

                    find["_id"] = {
                        "$gte": ObjectID.createFromTime(from.getTime() / 1000),
                        "$lte": ObjectID.createFromTime(to.getTime() / 1000)
                    };

                }

                //obter a lista de todas as partidas, agrupadas por jogo
                let cursor = await this.dbClient
                    .db(config.mongo.dbName)
                    .collection("match")
                    .aggregate([
                        {
                            $match: find
                        },
                        {
                            $group: {
                                _id: "$gameId",
                                count: {$sum: 1},
                                duration: {$sum: "$duration"}
                            }
                        }
                    ]);

                await cursor.forEach((r) => {

                    gameList.push(r._id.oid);

                    sum += r.count;
                    durationSum += r.duration;

                    match.push({
                        "_id": r._id.oid,
                        "name": gameRefList[r._id.oid.toString()].name,
                        "count": r.count,
                        "duration": r.duration
                    });

                });

            }

            return resolve({
                "total": sum,
                "totalDuration": durationSum,
                "games": match
            });

        });

    },
    /**
     * Interface para receber o nº total de acessos por contribuidor, dado um range de dados
     * @param {Date} from Data minima da partida
     * @param {Date} to Data máxima para a partida
     * @return {Promise<unknown>}
     */
    getGameTotalsPerContributor: async function(from, to) {

        return await new Promise(async (resolve, reject) => {

            let contributorList = [],
                users = await this.userController.getAllContributors(0, 0);

            if (users.length) {

                for(let i=0; i<users.length; i++) {

                    let stats = await this.getUserTotalsPerContributor(users[i]._id, from, to);

                    contributorList.push({
                        "_id": users[i]._id.toString(),
                        "name": users[i].name,
                        "count": stats.total,
                        "duration": stats.totalDuration
                    });

                }

            }

            return resolve({
                "contributor": contributorList
            });

        });

    }

};