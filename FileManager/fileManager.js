var imgur = require("imgur");
imgur.setClientId(process.env.CLIENTID);
imgur.setAPIUrl("https://api.imgur.com/3/");
imgur.setCredentials(
  process.env.EMAIL,
  process.env.IMGUR_PASSWORD,
  process.env.IMGUR_CLIENTID
);

function isImageValid(image){
    return image.match(urlRegex());
}

async function uploadBase64(image) {
  if (image && isImageValid(image)) {
    image = image.substring(image.indexOf(",") + 1);
    return await imgur
      .uploadBase64(image)
      .then(function(json) {
        console.log("json.data.link",json.data.link);
        return json.data.link ;
      })
      .catch(function(err) {
        console.log("err",err);
        return null;
      });
  }
  return null;
}

function urlRegex() {
  var expression = /^\s*data:([a-z]+\/[a-z]+(;[a-z\-]+\=[a-z\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/gi;
  var regex = new RegExp(expression);
  return regex;
}

module.exports.uploadBase64 = uploadBase64;
module.exports.isImageValid = isImageValid;
