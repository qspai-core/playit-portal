//get environment variables
const dotenv = require("dotenv");
dotenv.config();

const config = require("./config/settings"),
    express = require('express'),
    path = require('path'),
    log4js = require("log4js"),
    MongoClient = require("mongodb").MongoClient,
    ObjectID = require("mongodb").ObjectID,
    cookieParser = require('cookie-parser'),
    { body, query, param, validationResult, check } = require("express-validator"),
    { unprocessableEntityResponse, basicResponse, successfullyResponse } = require("./helpers/responseHelpers"),
    { isImageValid } = require("./FileManager/fileManager"),
    app = express();

// cors handling
if (process.env.NODE_ENV == "development") {
    const cors = require('cors');
    app.use(cors())
}

//load controllers
const authController = require("./api/controllers/auth"),
    userController = require("./api/controllers/user"),
    gameController = require("./api/controllers/game"),
    complaintController = require("./api/controllers/complaint"),
    statisticsController = require("./api/controllers/statistics");

log4js.configure(config.log4js.file);

const logger = log4js.getLogger("app");
logger.level = config.log4js.level;

const api = {
    /**
     * Função de inicialização da app
     * @returns {*}
     */
    init: function () {
        //ligar à BD
        this.connectToDB()
            .then((dbClient) => {

                logger.info("[API] server connect to MongoDB Atlas");

                authController.init(log4js, dbClient);
                userController.init(log4js, dbClient);
                gameController.init(log4js, dbClient);
                complaintController.init(log4js, dbClient);
                statisticsController.init(log4js, dbClient, gameController, userController);

                //carregar App (frontend e backend)
                this.loadApp();

            })
            .catch((e) => {
                logger.error("killing proccess:%s", e.message);
                //no caso de falha na ligação à BD, para o serviço para ser reiniciado posteriormente
                setTimeout(function () {
                    process.kill(process.pid, "SIGHUP");
                }, 1000);
            });

        return this;
    },
    /**
     * Função para carregar as rotas da API
     */
    loadApp: function () {

        //load views from REACT
        app.use(express.static(path.join(__dirname, 'frontend/build')));
        app.use(express.json({ limit: '50mb' }));
        app.use(express.urlencoded({ limit: '50mb', extended: false }));
        app.use(cookieParser());

        //rota para validar a autenticação de um utilizador
        app.post(
            "/auth",
            [
                body("user").isEmail(),
                body("password").isLength({ min: 1, max: 255 })
            ],
            this.handlers.auth
        );

        //rota para adicionar um novo utilizador
        app.post(
            "/user",
            [
                body("email").isEmail(),
                body("password").isLength({ min: 1, max: 255 }),
                body("name").isLength({ min: 3, max: 255 }),
                body("birth_date").isLength({ min: 10, max: 24 }),
                body("avatar").isLength({ min: 0 }),
                body("roleId").isMongoId()
            ],
            this.handlers.addUser
        );

        //rota para validar a conta de um utilizador
        app.put(
            "/user/validate/:id",
            [
                param("id").isMongoId()
            ],
            this.handlers.userValidateById
        );

        //rota para alterar a password de um utilizador
        app.put(
            "/user/password/:id",
            [
                param("id").isMongoId(),
                body("password").isLength({ min: 1, max: 255 }),
            ],
            this.handlers.userPasswordById
        );

        //rota para alterar as informações de um utilizador
        app.put(
            "/user/:id",
            [
                param("id").isMongoId(),
                body("avatar").custom(value => {
                    if (value && !isImageValid(value)) {
                        return Promise.reject('Avatar in incorret format');
                    }
                    return true;
                }),
                check(
                    'new_password',
                    'confirm_password field must have the same value as the new_password field',
                )
                    .custom((value, { req }) => value === req.body.confirm_password),
            ],
            this.handlers.updateUserById
        );

        //rota para proceder à recuperação da password de um utilizador
        app.put(
            "/user/recover/:email",
            [
                param("email").isEmail()
            ],
            this.handlers.userRecoverByEmail
        );

        //rota para obter os dados de um utilizador
        app.get(
            "/user/:id",
            [
                param("id").isMongoId()
            ],
            this.handlers.getUserById
        );

        //rota para obter os dados de todos os utilizadores
        app.get(
            "/user/all/:skip/:limit",
            [
                param("skip").isInt({ min: 0 }),
                param("limit").isInt({ min: 0 })
            ],
            this.handlers.getAllUsersPaging
        );

        //Rota para obter os dados de todos os contribuidores
        app.get("/contributors/all/:skip/:limit",
            [
                param("skip").isInt({ min: 0 }),
                param("limit").isInt({ min: 0 })
            ],
            this.handlers.getAllContributorsPaging
        );

        //Rota para obter os dados de todos os jogadores
        app.get("/players/all/:skip/:limit",
            [
                param("skip").isInt({ min: 0 }),
                param("limit").isInt({ min: 0 })
            ],
            this.handlers.getAllPlayersPaging
        );
        //rota para alterar os dados de um utilizador
        app.put(
            "/user/profile/:id",
            [
                param("id").isMongoId(),
                body("name").isLength({ min: 3, max: 255 }),
                body("birth_date").isLength({ min: 10, max: 24 }),
                body("avatar").isLength({ min: 0 }),
                body("roleId").isMongoId()
            ],
            this.handlers.userProfileById
        );

        //rota para validar a conta de um utilizador
        app.put(
            "/user/active/:id",
            [
                param("id").isMongoId(),
                body("active").isBoolean()
            ],
            this.handlers.userActiveById
        );

        //rota para obter os dados de todos os jogos
        app.get(
            "/game/all/:skip/:limit",
            [
                param("skip").isInt({ min: 0 }),
                param("limit").isInt({ min: 0 })
            ],
            this.handlers.getAllGamesPaging
        );

        //rota para obter os dados de um jogo
        app.get(
            "/game/:id",
            [
                param("id").isMongoId()
            ],
            this.handlers.gameById
        );

        //rota para ativar/destativar um jogo
        app.put(
            "/game/active/:id",
            [
                param("id").isMongoId(),
                body("active").isBoolean()
            ],
            this.handlers.activeGameById
        );

        //rota para adicionar um jogo
        app.post(
            "/game",
            [              
                body("image").custom(value => {
                    if (value && !isImageValid(value)) {
                        return Promise.reject('Image in incorret format');
                    }
                    return true;
                }),
                body("name").isLength({ min: 3, max: 255 }),
                body("url").isLength({ min: 10, max: 255 }),
                body("ownerId").isMongoId(),
            ],
            this.handlers.addGame
        );

        //rota para obter os dados de todos as denuncias
        app.get(
            "/complaint/all/:skip/:limit",
            [
                param("skip").isInt({ min: 0 }),
                param("limit").isInt({ min: 0 })
            ],
            this.handlers.getAllComplaintPaging
        );

        //rota para obter os dados de uma denuncia
        app.get(
            "/complaint/:id",
            [
                param("id").isMongoId()
            ],
            this.handlers.getComplaintById
        );

        //rota para obter denuncias de um utilizador
        app.get("/complaint/user/:id",
            [
                param("id").isMongoId()
            ],
            this.handlers.getComplaintByUserId
        )

        //rota para arquivar uma denuncia
        app.put(
            "/complaint/archived/:id",
            [
                param("id").isMongoId(),
                body("userId").isMongoId(),
                body("archived").isBoolean()
            ],
            this.handlers.archiveComplainById
        );

        //rota para adicionar uma resposta a uma denuncia
        app.post(
            "/complaint/response/:id",
            [
                param("id").isMongoId(),
                body("userId").isMongoId(),
                body("response").isLength({ min: 1, max: 255 })
            ],
            this.handlers.addComplaintResponseById
        );

        //rota para adicionar uma nova denuncia
        app.post(
            "/complaint",
            [
                body("userId").isMongoId(),
                body("gameId").isMongoId(),
                body("title").isLength({ min: 1, max: 50 }),
                body("description").isLength({ min: 1, max: 255 })
            ],
            this.handlers.addComplaint
        );

        //Rota que retorna o total de acessos à plataforma por jogo ou todos os jogos
        app.get(
            "/statistics/game",
            [
                query("gameId").custom(value => {

                    if(!value)
                        return true;

                    try {
                        new ObjectID(value);
                    } catch (e) {
                        return Promise.reject('Invalid value');
                    }

                    return true;
                }),
                query("from").isDate({format:'YYYY-MM-DD'}),
                query("to").isDate({format:'YYYY-MM-DD'})
            ],
            this.handlers.gameStatistics
        );

        //Rota que retorna o total de acessos à plataforma por jogo ou todos os jogos para um utilizador
        app.get(
            "/statistics/user",
            [
                query("from").isDate({format:'YYYY-MM-DD'}),
                query("to").isDate({format:'YYYY-MM-DD'})
            ],
            this.handlers.userStatistics
        );

        //Rota que fornece o nº de jogadores por jogo de um determinado contribuidor
        app.get(
            "/statistics/user/:contributorId",
            [
                param("contributorId").isMongoId()
            ],
            this.handlers.userStatisticsPerContributor
        );

        //Rota para receber o nº total de acessos por contribuidor, dado um range de dados
        app.get(
            "/statistics/game/contributor",
            [
                query("from").isDate({format:'YYYY-MM-DD'}),
                query("to").isDate({format:'YYYY-MM-DD'})
            ],
            this.handlers.gameStatisticsPerContributor
        );

    },
    handlers: {
        /**
         * Handler para validar a autenticação de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        auth: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //efetuar chamada para validar autenticação
            authController.validate(req.body["user"], req.body["password"], function (err, user) {
                return basicResponse(res, err, user);
            });

        },
        /**
         * Handler para adicionar um novo utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        addUser: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //efetuar chamada para validar autenticação
            userController.addUser(req.body["email"], req.body["password"], req.body["name"], new Date(req.body["birth_date"]), req.body["avatar"], req.body["roleId"], function (err, id) {
                return basicResponse(res, err, { _id: id });
            });

        },
        /**
         * Handler para validar a conta de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        userValidateById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //efetuar chamada para validar autenticação
            userController.validateAccount(req.params.id, function (err) {
                return basicResponse(res, err);
            });

        },
        /**
         * rota para alterar a password de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        userPasswordById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //efetuar chamada para atualizar a password
            userController.setPassword(req.params.id, req.body["password"], function (err) {
                return basicResponse(res, err);
            });

        },
        /**
         * Handler para alterar as informações de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        updateUserById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //valida se a suposta password antiga é igual à que está na bd
            authController.validatePassword(new ObjectID(req.params.id), req.body["old_password"], function (err, user) {

                if (err)
                    return res.status(400).json({
                        success: false,
                        error: err
                    });

                //efetuar chamada para atualizar a password
                userController.updatePasswordAndProfilePicture(req.params.id, req.body["new_password"], req.body["avatar"], function (error) {
                    return basicResponse(res, error);
                });

            });
        },
        /**
         * Handler para proceder à recuperação da password de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        userRecoverByEmail: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //efetuar chamada para atualizar a password
            userController.sendRecoverMail(req.params.email, function (err) {
                return basicResponse(res, err);
            });

        },
        /**
         * Handler para obter os dados de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getUserById: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            const user = await userController.getUserById(new ObjectID(req.params.id));
            if (!user) {
                return res.status(400).json({
                    errors: {
                        "success": false,
                        "error": {
                            "code": 2,
                            "message": "Utilizador não existe"
                        }
                    }
                });
            }

            delete user.role;
            return successfullyResponse(res, user);
        },
        /**
         * rota para obter os dados de todos os utilizadores
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getAllUsersPaging: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            const users = await userController.getAllUsers(Number(req.params.skip), Number(req.params.limit));

            return successfullyResponse(res, users);
        },
        /**
         * Handler para obter os dados de todos os contribuidores
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getAllContributorsPaging: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }


            const users = await userController.getAllContributors(Number(req.params.skip), Number(req.params.limit));

            return successfullyResponse(res, users);
        },
        /**
         * Handler para obter os dados de todos os jogadores
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getAllPlayersPaging: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }


            const users = await userController.getAllPlayers(Number(req.params.skip), Number(req.params.limit));

            return successfullyResponse(res, users);
        },
        /**
         * Handler para alterar os dados de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        userProfileById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //efetuar chamada para atualizar a password
            userController.setUser(req.params.id, req.body["name"], new Date(req.body["birth_date"]), req.body["avatar"], req.body["roleId"], function (err) {
                return basicResponse(res, err);
            });
        },
        /**
         * Handler para validar a conta de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        userActiveById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //definir o estado do utilizador
            userController.setActive(req.params.id, eval(req.body["active"]), function (err) {
                return basicResponse(res, err);
            });

        },
        /**
         * Handler para obter os dados de todos os jogos
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getAllGamesPaging: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            const games = await gameController.getAllGames(Number(req.params.skip), Number(req.params.limit));

            return successfullyResponse(res, games);
        },
        /**
         * Handler rota para obter os dados de um jogo
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        gameById: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            const game = await gameController.getGameById(new ObjectID(req.params.id));

            if (!game) {
                return res.status(400).json({
                    errors: {
                        "success": false,
                        "error": {
                            "code": 2,
                            "message": "Jogo não existe"
                        }
                    }
                });
            }

            return successfullyResponse(res, game);
        },
        /**
         * Handler para ativar/destativar um jogo
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        activeGameById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //definir o estado do jogo
            gameController.setActive(req.params.id, eval(req.body["active"]), function (err) {
                return basicResponse(res, err);
            });

        },
        /**
         * Handler para adicionar um jogo
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        addGame: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //definir o estado do jogo
            gameController.addGame(req.body["image"], req.body["name"], req.body["url"], req.body["ownerId"], function (err) {
                return basicResponse(res, err);
            });
        },
        /**
         * Handler para obter os dados de todos as denuncias
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getAllComplaintPaging: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            const complaint = await complaintController.getAllComplaint(Number(req.params.skip), Number(req.params.limit));

            return successfullyResponse(res, complaint);
        },
        /**
         * Handler para obter os dados de uma denuncia
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getComplaintById: async (req, res) => {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            const complaint = await complaintController.getComplaintById(new ObjectID(req.params.id));

            if (!complaint) {
                return res.status(400).json({
                    errors: {
                        "success": false,
                        "error": {
                            "code": 2,
                            "message": "Denuncia não existe"
                        }
                    }
                });
            }

            return successfullyResponse(res, complaint);
        },
        /**
         * Handler para obter denuncias de um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        getComplaintByUserId: async (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            const complaint = await complaintController.getComplaintByUserId(new ObjectID(req.params.id));

            if (!complaint) {
                return res.status(400).json({
                    errors: {
                        "success": false,
                        "error": {
                            "code": 2,
                            "message": "Denuncia não existe"
                        }
                    }
                });
            }

            return successfullyResponse(res, complaint);
        },
        /**
         * Handler para arquivar uma denuncia
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        archiveComplainById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //definir o estado de arquivo de uma denuncia
            complaintController.setArchived(req.params.id, req.body["userId"], eval(req.body["archived"]), function (err) {
                return basicResponse(res, err);
            });

        },
        /**
         * Handler para adicionar uma resposta a uma denuncia
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {*}
         */
        addComplaintResponseById: (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //definir o estado de arquivo de uma denuncia
            complaintController.addResponse(req.params.id, req.body["userId"], req.body["response"], function (err) {
                return basicResponse(res, err);
            });

        },
        /**
         * Handler para adicionar uma nova denuncia
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        addComplaint: async (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            let _idGame = null;

            try {

                _idGame = new ObjectID(req.body["gameId"]);

            } catch (e) {

                return basicResponse(res, {
                    code: 7,
                    message: `Jogo gameId ${req.body["gameId"]} não é um ObjectId válido`
                });

            }

            const game = await gameController.getGameById(_idGame);

            if (!game) {
                return res.status(400).json({
                    errors: {
                        "success": false,
                        "error": {
                            "code": 2,
                            "message": "Jogo não existe"
                        }
                    }
                });
            }

            //adicionar a denuncia
            complaintController.addComplaint(req.body["userId"], req.body["gameId"], req.body["title"], req.body["description"])
                .then((id) => {
                    return basicResponse(res, null, id);
                })
                .catch((err) => {
                    return basicResponse(res, err);
                });

        },
        /**
         * Handler que retorna o total de acessos à plataforma por jogo ou todos os jogos
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        gameStatistics: async (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            let _idGame = !req.query["gameId"] ? null : new ObjectID(req.query["gameId"]);

            //o idGame não é um campo obrigatório
            if(_idGame) {
                const game = await gameController.getGameById(_idGame);

                if (!game) {
                    return res.status(400).json({
                        errors: {
                            "success": false,
                            "error": {
                                "code": 2,
                                "message": "Jogo não existe"
                            }
                        }
                    });
                }
            }

            //adicionar a denuncia
            statisticsController.getGameTotals(_idGame, new Date(req.query["from"]), new Date(req.query["to"]))
                .then((rst) => {
                    return basicResponse(res, null, rst);
                })
                .catch((err) => {
                    return basicResponse(res, err);
                });

        },
        /**
         * Handler para receber o nº total de acessos por contribuidor, dado um range de dados
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        gameStatisticsPerContributor: async (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //adicionar a denuncia
            statisticsController.getGameTotalsPerContributor(new Date(req.query["from"]), new Date(req.query["to"]))
                .then((rst) => {
                    return basicResponse(res, null, rst);
                })
                .catch((err) => {
                    return basicResponse(res, err);
                });

        },
        /**
         * Handler que retorna o total de acessos à plataforma por jogo ou todos os jogos para um utilizador
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        userStatistics: async (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //adicionar a denuncia
            statisticsController.getUserTotals(new Date(req.query["from"]), new Date(req.query["to"]))
                .then((rst) => {
                    return basicResponse(res, null, rst);
                })
                .catch((err) => {
                    return basicResponse(res, err);
                });

        },
        /**
         * Handler que fornece o nº de jogadores por jogo de um determinado contribuidor
         * @param req The req object represents the HTTP request
         * @param res Response The res object represents the HTTP response
         * @return {Promise<*>}
         */
        userStatisticsPerContributor: async (req, res) => {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return unprocessableEntityResponse(res, errors);
            }

            //adicionar a denuncia
            statisticsController.getUserTotalsPerContributor(new ObjectID(req.params.contributorId), null, null)
                .then((rst) => {
                    return basicResponse(res, null, rst);
                })
                .catch((err) => {
                    return basicResponse(res, err);
                });

        }
    },
    /**
     * Função para se ligar à MongoDB
     * @returns {Promise<unknown>}
     */
    connectToDB: function () {

        return new Promise((resolve, reject) => {
            this.dbClient = new MongoClient(config.mongo.connectionString, {
                useUnifiedTopology: true,
            });

            // Use connect method to connect to the Server
            this.dbClient.connect(function (err, dbClient) {
                if (err) {
                    logger.error("MongoDB connection error: %s", err.message);

                    reject();
                    return;
                }

                logger.info("MongoDB connected successfully");

                resolve(dbClient);
            });
        });

    }

}.init();

module.exports = app;
